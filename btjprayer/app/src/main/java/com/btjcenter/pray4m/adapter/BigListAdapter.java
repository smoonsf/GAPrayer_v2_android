package com.btjcenter.pray4m.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.activity.PostActivity;
import com.btjcenter.pray4m.model.Post;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by alexmoon on 2015. 11. 9..
 */
public class BigListAdapter extends RecyclerView.Adapter<BigListAdapter.ViewHolder> {
    List<Post> posts;
    View rootView;

    class ViewHolder extends RecyclerView.ViewHolder{
        public TextView m_title;
        public TextView m_date;
        public ImageView m_image;
        public ViewHolder(View v) {
            super(v);
            this.m_title = (TextView) v.findViewById(R.id.title_biglist_item);
            this.m_date = (TextView) v.findViewById(R.id.date_biglist_item);
            this.m_image = (ImageView) v.findViewById(R.id.image_biglist_item);
        }
    }

    public BigListAdapter(List<Post> postlist, View rv){
        posts = postlist;
        rootView = rv;
    }

    @Override
    public BigListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder =
                new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_biglist, null));


        return holder;
    }

    @Override
    public void onBindViewHolder(BigListAdapter.ViewHolder holder, int position) {
        final Post post = posts.get(position);
        holder.m_title.setText(post.getTitle_plain());
        holder.m_date.setText(post.getDate().substring(0, 10));
        if(post.getAttachments().size() != 0)
            Picasso.with(rootView.getContext())
                    .load(post.getAttachments().get(0).getUrl())
                    .placeholder(R.drawable.temp)
                    .into(holder.m_image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(rootView.getContext(), PostActivity.class);
                intent.putExtra("id", post.getId());

                rootView.getContext().startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return posts.size();
    }


}
