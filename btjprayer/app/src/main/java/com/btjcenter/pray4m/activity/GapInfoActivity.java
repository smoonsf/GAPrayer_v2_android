package com.btjcenter.pray4m.activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.btjcenter.pray4m.R;

public class GapInfoActivity extends BTJPrayerBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gapinfo);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.97);
        getWindow().setLayout(screenWidth, WindowManager.LayoutParams.MATCH_PARENT);
    }
}
