package com.btjcenter.pray4m.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.adapter.RamadanPagerAdapter;

import java.util.Calendar;

/**
 * Created by alexmoon on 2016. 5. 10..
 */
public class RamadanActivity extends BTJPrayerBaseActivity {
    final private int LANG_KOR = 0;
    final private int LANG_ENG = 1;
    final private int LANG_CN = 2;
    final private int LANG_JAP = 3;
    private int language_mode = 0;

    private int current_page = 0;


    ViewPager viewPager;
    RamadanPagerAdapter adapter;
    TabLayout tabLayout;
    TextView text_kor, text_eng, text_cn, text_jap;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ramadan);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.97);
        getWindow().setLayout(screenWidth, WindowManager.LayoutParams.MATCH_PARENT);
        Calendar current = Calendar.getInstance();


        viewPager = (ViewPager)findViewById(R.id.pager_ramadan);
        tabLayout = (TabLayout)findViewById(R.id.tabs_ramadan);
        text_kor =  (TextView)findViewById(R.id.text_kor);
        text_eng = (TextView)findViewById(R.id.text_eng);
        text_cn = (TextView)findViewById(R.id.text_cn);
        text_jap = (TextView)findViewById(R.id.text_jap);



        adapter = new RamadanPagerAdapter(getSupportFragmentManager(), language_mode);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        changeTabsFont(tabLayout);


        text_kor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                language_mode = LANG_KOR;

                adapter = new RamadanPagerAdapter(getSupportFragmentManager(), language_mode);
                viewPager.setAdapter(adapter);
                viewPager.setCurrentItem(current_page);
            }
        });
        text_eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                language_mode = LANG_ENG;

                adapter = new RamadanPagerAdapter(getSupportFragmentManager(), language_mode);
                viewPager.setAdapter(adapter);
                viewPager.setCurrentItem(current_page);
            }
        });
        text_cn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                language_mode = LANG_CN;

                adapter = new RamadanPagerAdapter(getSupportFragmentManager(), language_mode);
                viewPager.setAdapter(adapter);
                viewPager.setCurrentItem(current_page);
            }
        });
        text_jap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                language_mode = LANG_JAP;

                adapter = new RamadanPagerAdapter(getSupportFragmentManager(), language_mode);
                viewPager.setAdapter(adapter);
                viewPager.setCurrentItem(current_page);
            }
        });


        viewPager.setCurrentItem(adapter.getCount() - 1);
        current_page = viewPager.getCurrentItem();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int prev = current_page;
                current_page = position;
                if(prev==0&&position!=0){
                    Intent toDayTab = new Intent("TO_DAY_TAB");
                    sendBroadcast(toDayTab);
                }
                if(prev!=0&&position==0){
                    Intent toVideoTab = new Intent("TO_VIDEO_TAB");
                    sendBroadcast(toVideoTab);
                }
            }

            @Override
            public void onPageSelected(int position) {
                int prev = current_page;
                current_page = position;
                if(prev==0&&position!=0){
                    Intent toDayTab = new Intent("TO_DAY_TAB");
                    sendBroadcast(toDayTab);
                }
                if(prev!=0&&position==0){
                    Intent toVideoTab = new Intent("TO_VIDEO_TAB");
                    sendBroadcast(toVideoTab);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        adapter = new RamadanPagerAdapter(getSupportFragmentManager(), language_mode);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(current_page);
    }

    private void changeTabsFont(TabLayout tabLayout) {
        Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/NanumBarunGothic.otf");
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(font, Typeface.NORMAL);
                }
            }
        }
    }
}
