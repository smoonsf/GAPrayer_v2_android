package com.btjcenter.pray4m.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.activity.PostActivity;
import com.btjcenter.pray4m.model.Comment;

import java.util.List;

/**
 * Created by alexmoon on 2015. 11. 9..
 */
public class HomeCommentListAdapter extends RecyclerView.Adapter<HomeCommentListAdapter.ViewHolder> {
    List<Comment> comments;

    class ViewHolder extends RecyclerView.ViewHolder{
        public TextView m_parent_title;
        public TextView m_name;
        public TextView m_content;
        public ViewHolder(View v) {
            super(v);
            this.m_parent_title = (TextView) v.findViewById(R.id.parent_title_homecommentitem);
            this.m_name = (TextView) v.findViewById(R.id.text_name_homecommentitem);
            this.m_content = (TextView) v.findViewById(R.id.text_content_homecommentitem);
        }
    }

    public HomeCommentListAdapter(List<Comment> commentslist){
        comments = commentslist;
    }

    @Override
    public HomeCommentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder =
                new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_homecommentlist, null));

        return holder;
    }

    @Override
    public void onBindViewHolder(HomeCommentListAdapter.ViewHolder holder, int position) {
        final Comment comment = comments.get(position);
        holder.m_name.setText(comment.getName());
        holder.m_content.setText(comment.getContent());
        String parent_title = "";
        switch (comment.getCategory()){
            case "today":
                parent_title = "[오늘의 기도] "+comment.getParent_title(); break;
            case "nationsjoin":
                parent_title = "[종족별 기도] "+comment.getParent_title().split(";")[0];; break;
            case "columns":
                parent_title = "[BTJ 자료실] "+comment.getParent_title(); break;
            case "gap":
                parent_title = "[GAP 말씀] "+comment.getParent_title(); break;
            case "nations":
                parent_title = "[종족 기도] "+comment.getParent_title(); break;
            case "16p4m":
            case "16p4meng":
            case "16p4mjap":
            case "16p4mcn":
                parent_title = "[PRAY FOR MUSLIMS] "+comment.getParent_title(); break;
        }

        holder.m_parent_title.setText(parent_title);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), PostActivity.class);
                intent.putExtra("id", comment.getParent());
                if(comment.getCategory().equals("nationsjoin"))
                    intent.putExtra("nation", true);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }


}
