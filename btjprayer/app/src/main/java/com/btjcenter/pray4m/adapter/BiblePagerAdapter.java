package com.btjcenter.pray4m.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.btjcenter.pray4m.BiblePageholderFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexmoon on 2015. 11. 13..
 */
public class BiblePagerAdapter extends FragmentStatePagerAdapter {

    int[] book;
    int[] chapter;
    int[] init;
    List<ArrayList<String>> verses;
    FragmentManager fragmentManager;
    ArrayList<BiblePageholderFragment> fragmentInstances = new ArrayList<>();

    public BiblePagerAdapter(FragmentManager fm, int[] _book, int[] _chapter, int[] _init, List<ArrayList<String>> _verses) {
        super(fm);
        fragmentManager = fm;
        book = _book;
        chapter = _chapter;
        init = _init;
        verses = _verses;

        for (int i = 0; i < book.length + 1; i++) {
            BiblePageholderFragment fragment;

            if (i == book.length)
                fragment = BiblePageholderFragment.create();
            else
                fragment = BiblePageholderFragment.create(init[i], verses.get(i), getPageTitle(i).toString());

            fragmentInstances.add(fragment);
        }
    }

    public BiblePageholderFragment getFragmentItem(int index) {
        return fragmentInstances.get(index);
    }

    @Override
    public Fragment getItem(int position) {


        return fragmentInstances.get(position);
    }

    @Override
    public int getCount() {
        return book.length + 1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == book.length)
            return "GAP성경읽기표";

        return getBookName(position);
    }

    private CharSequence getBookName(int position) {
        String title = null;
        switch (book[position]) {
            case 1:
                title = "창세기";
                break;
            case 2:
                title = "출애굽기";
                break;
            case 3:
                title = "레위기";
                break;
            case 4:
                title = "민수기";
                break;
            case 5:
                title = "신명기";
                break;
            case 6:
                title = "여호수아";
                break;
            case 7:
                title = "사사기";
                break;
            case 8:
                title = "룻기";
                break;
            case 9:
                title = "사무엘상";
                break;
            case 10:
                title = "사무엘하";
                break;
            case 11:
                title = "열왕기상";
                break;
            case 12:
                title = "열왕기하";
                break;
            case 13:
                title = "역대상";
                break;
            case 14:
                title = "역대하";
                break;
            case 15:
                title = "에스라";
                break;
            case 16:
                title = "느헤미야";
                break;
            case 17:
                title = "에스더";
                break;
            case 18:
                title = "욥기";
                break;
            case 19:
                title = "시편";
                break;
            case 20:
                title = "잠언";
                break;
            case 21:
                title = "전도서";
                break;
            case 22:
                title = "아가";
                break;
            case 23:
                title = "이사야";
                break;
            case 24:
                title = "예레미야";
                break;
            case 25:
                title = "예레미야애가";
                break;
            case 26:
                title = "에스겔";
                break;
            case 27:
                title = "다니엘";
                break;
            case 28:
                title = "호세아";
                break;
            case 29:
                title = "요엘";
                break;
            case 30:
                title = "아모스";
                break;
            case 31:
                title = "오바댜";
                break;
            case 32:
                title = "요나";
                break;
            case 33:
                title = "미가";
                break;
            case 34:
                title = "나훔";
                break;
            case 35:
                title = "하박국";
                break;
            case 36:
                title = "스바냐";
                break;
            case 37:
                title = "학개";
                break;
            case 38:
                title = "스가랴";
                break;
            case 39:
                title = "말라기";
                break;
            case 40:
                title = "마태복음";
                break;
            case 41:
                title = "마가복음";
                break;
            case 42:
                title = "누가복음";
                break;
            case 43:
                title = "요한복음";
                break;
            case 44:
                title = "사도행전";
                break;
            case 45:
                title = "로마서";
                break;
            case 46:
                title = "고린도전서";
                break;
            case 47:
                title = "고린도후서";
                break;
            case 48:
                title = "갈라디아서";
                break;
            case 49:
                title = "에베소서";
                break;
            case 50:
                title = "빌립보서";
                break;
            case 51:
                title = "골로새서";
                break;
            case 52:
                title = "데살로니가전서";
                break;
            case 53:
                title = "데살로니가후서";
                break;
            case 54:
                title = "디모데전서";
                break;
            case 55:
                title = "디모데후서";
                break;
            case 56:
                title = "디도서";
                break;
            case 57:
                title = "빌레몬서";
                break;
            case 58:
                title = "히브리서";
                break;
            case 59:
                title = "야고보서";
                break;
            case 60:
                title = "베드로전서";
                break;
            case 61:
                title = "베드로후서";
                break;
            case 62:
                title = "요한1서";
                break;
            case 63:
                title = "요한2서";
                break;
            case 64:
                title = "요한3서";
                break;
            case 65:
                title = "유다서";
                break;
            case 66:
                title = "요한계시록";
                break;
        }

        if (book[position] == 19)
            title += " " + chapter[position] + "편";
        else
            title += " " + chapter[position] + "장";


        return title;
    }
}
