package com.btjcenter.pray4m.activity;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.btjcenter.pray4m.R;

import java.io.IOException;

public class WakeActivity extends BTJPrayerBaseActivity {
    static public String TAG = "WakeActivity";

    Vibrator vibrator;
    AudioManager audioManager;

    MediaPlayer mediaPlayer;
    SoundPool soundPool;
    int sound_id;

    Button button_ok, button_close;
    TextView textView_memo, textView_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wake);
        final Context context = this;

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.7);
        int screenHeight = (int) (metrics.heightPixels * 0.5);
        getWindow().setLayout(screenWidth, screenHeight);


//        soundPool = new SoundPool(1, AudioManager.STREAM_ALARM, 0);
//        sound_id = soundPool.load(this, R.raw.doorbell, 1);

        audioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
        vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);


        long[] pattern = {1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000
                , 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000};

        vibrator.vibrate(pattern, -1);vibrator.vibrate(pattern, -1);
        //soundPool.play(sound_id, 1, 1, 1, 30, 1);

        Uri ring = Uri.parse("android.resource://com.btjcenter.pray4m/" + R.raw.doorbell);
        mediaPlayer = new MediaPlayer();

        if(audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
            vibrator.vibrate(pattern, -1);
            try {
                mediaPlayer.setDataSource(this, ring);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mediaPlayer.setLooping(true);
                mediaPlayer.prepare();
                mediaPlayer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
            vibrator.vibrate(pattern, -1);
        }




        Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        String memo = "";
        memo += getIntent().getStringExtra("memo");
        textView_memo = (TextView)findViewById(R.id.memo_wake);
        textView_memo.setText(memo);

        textView_time = (TextView) findViewById(R.id.time_wake);
        textView_time.setText(getIntent().getStringExtra("time"));



        button_ok = (Button) findViewById(R.id.button_ok_wake);
        button_close = (Button) findViewById(R.id.button_cancel_wake);

        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //soundPool.stop(sound_id);
                mediaPlayer.stop();
                vibrator.cancel();
                Intent intent = new Intent(context, SplashActivity.class);
                startActivity(intent);
                finish();
            }
        });

        button_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //soundPool.stop(sound_id);
                mediaPlayer.stop();
                vibrator.cancel();
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        mediaPlayer.stop();
        vibrator.cancel();
    }

    @Override
    protected void onDestroy() {
        //soundPool.stop(sound_id);
        mediaPlayer.stop();
        vibrator.cancel();
        super.onDestroy();
    }
}
