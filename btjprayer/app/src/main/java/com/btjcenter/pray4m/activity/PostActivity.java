package com.btjcenter.pray4m.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.btjcenter.pray4m.DividerItemDecoration;
import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.adapter.CommentListAdapter;
import com.btjcenter.pray4m.api.ListDeserializer;
import com.btjcenter.pray4m.api.PostService;
import com.btjcenter.pray4m.model.Comment;
import com.btjcenter.pray4m.model.Post;
import com.btjcenter.pray4m.utils.DateProvider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
//import com.kakao.kakaolink.KakaoLink;
//import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;
//import com.kakao.util.KakaoParameterException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class PostActivity extends BTJPrayerBaseActivity {

    private static final String TAG = "PostActivity";

    Boolean nation;

    //KakaoLink kakaoLink;
    //KakaoTalkLinkMessageBuilder kakaoTalkLinkMessageBuilder;

    int id;

    WebView webView;
    Button submit_comment;

    List<Comment> comments;
    RecyclerView comments_list;
    LinearLayoutManager layoutManager;
    CommentListAdapter adapter;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        id = getIntent().getIntExtra("id", 0);
        //id = 8998;
        nation = getIntent().getBooleanExtra("nation", false);
        final Context context = this;


//        try {
//            kakaoLink = KakaoLink.getKakaoLink(this);
//        } catch (KakaoParameterException e) {
//            e.printStackTrace();
//        }
//        kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();



        submit_comment = (Button) findViewById(R.id.btn_submit);
        submit_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CommentActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });
        submit_comment.setVisibility(View.INVISIBLE);


        comments_list = (RecyclerView) findViewById(R.id.recycler_post);
        comments_list.fling(1, 1);
        comments_list.setVisibility(View.INVISIBLE);
        layoutManager = new LinearLayoutManager(this);
        comments_list.setLayoutManager(layoutManager);
        comments_list.setHasFixedSize(false);
        comments_list.addItemDecoration(new DividerItemDecoration(this));

        comments = new ArrayList<Comment>();

        adapter = new CommentListAdapter(comments);
        comments_list.setAdapter(adapter);



        webView = (WebView)findViewById(R.id.webview_post);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                view.reload();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                comments_list.setVisibility(View.VISIBLE);
                comments_list.stopScroll();
                submit_comment.setVisibility(View.VISIBLE);
            }
        });

        Log.w(TAG, "id : " + id + "// nation : " + nation);
        String url = "http://www.btjprayer.net/archives/" + id + "?mobile=true";
        if(nation)
            url += "&nation=true";


        webView.loadUrl(url);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_post);
        if(toolbar != null)
            toolbar.setTitle("");
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setPadding(30,30,30,30);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GapActivity.class);
                startActivity(intent);
            }
        });

        FloatingActionButton fab_ramadan = (FloatingActionButton) findViewById(R.id.fab_ramadan);
        fab_ramadan.setPadding(15,20,10,20);
        fab_ramadan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RamadanActivity.class);
                startActivity(intent);
            }
        });
        if(Calendar.getInstance().after(new DateProvider().RAMADAN16_END))
            fab_ramadan.setVisibility(View.GONE);


    }

    @Override
    protected void onResume() {
        super.onResume();
        comments.clear();
        Type type = new TypeToken<Post>(){}.getType();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(type, new ListDeserializer<>(Post.class, "post")).create();;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.btjprayer.net")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        PostService postService = retrofit.create(PostService.class);

        final Call<Post> postcall = postService.getPost(id);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                postcall.enqueue(new Callback<Post>() {
                    @Override
                    public void onResponse(final Response<Post> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            comments.addAll(response.body().getComments());
                            //Log.d(TAG, comments.size() + "");
                            adapter.notifyDataSetChanged();
                            //layoutManager.notify();
                            if(getSupportActionBar() != null) {

                                if (response.body().getCategories().size() != 0) {
                                    switch (response.body().getCategories().get(0).getSlug()) {
                                        case "today":
                                            getSupportActionBar().setTitle("오늘의 기도");
                                            break;
                                        case "nations":
                                        case "nationsjoin":
                                            getSupportActionBar().setTitle("종족별 기도");
                                            break;
                                        case "columns":
                                            getSupportActionBar().setTitle("열방 자료실");
                                        case "16p4m":
                                        case "16p4meng":
                                        case "16p4mcn":
                                        case "16p4mjap":
                                            getSupportActionBar().setTitle("[PRAY FOR MUSLIMS]");
                                            break;
                                    }
                                }
                            }


//                    try {
//                        if (response.body().getAttachments().size() != 0)
//                            kakaoTalkLinkMessageBuilder.addImage(response.body().getAttachments().get(0).getUrl(), 100, 100)
//                                    .addText(response.body().getTitle_plain())
//                                    .addAppButton("앱으로 이동", new AppActionBuilder()
//                                            .addActionInfo(AppActionInfoBuilder
//                                                    .createAndroidActionInfoBuilder()
//                                                    .setExecuteParam("id=" + id)
//                                                    .build())
//                                            .setUrl("http://www.prayformuslims.org")
//                                            .build())
//                                    .build();
//                        else
//                            kakaoTalkLinkMessageBuilder.addText(response.body().getTitle_plain())
//                                    .addAppButton("앱으로 이동", new AppActionBuilder().setUrl("http://www.prayformuslims.org").build())
//                                    .build();
//
//                    } catch (KakaoParameterException e) {
//                        e.printStackTrace();
//                    }
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                    }
                });
            }
        };

        Handler handler = new Handler();
        handler.postDelayed(runnable, 1000);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (item_id == R.id.action_share) {
//            try {
//                kakaoLink.sendMessage(kakaoTalkLinkMessageBuilder, this);
//            } catch (KakaoParameterException e) {
//                e.printStackTrace();
//            }
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("BTJ Prayer", "http://www.btjprayer.net/archives/" + id);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(this, "링크가 복사되었습니다", Toast.LENGTH_SHORT).show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
