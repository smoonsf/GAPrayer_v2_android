package com.btjcenter.pray4m.adapter;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.btjcenter.pray4m.R;

import java.util.ArrayList;

/**
 * Created by alexmoon on 2015. 11. 9..
 */
public class BibleListAdapter extends RecyclerView.Adapter<BibleListAdapter.ViewHolder> {
    private static final String TAG = "BibleListAdapter";

    private String tabTitle;
    ArrayList<String> content;
    int init;
    ArrayList<Boolean> copyChecked;

    Context mContext;

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView m_verse;
        public TextView m_content;

        public ViewHolder(View v) {
            super(v);
            this.m_verse = (TextView) v.findViewById(R.id.verse_bible);
            this.m_content = (TextView) v.findViewById(R.id.content_bible);
        }
    }

    public BibleListAdapter(Context context, int _init, ArrayList<String> _content, String tabTitle) {
        mContext = context;
        content = _content;
        init = _init;
        this.tabTitle = tabTitle;
        copyChecked = new ArrayList<>();
        for (int i = 0; i < content.size(); i++)
            copyChecked.add(false);
    }

    @Override
    public BibleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder =
                new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bible, null));


        return holder;
    }

    @Override
    public void onBindViewHolder(final BibleListAdapter.ViewHolder holder, final int position) {
        holder.m_verse.setText("" + (init + position));

        final UnderlineSpan underlineSpan = new UnderlineSpan();
        TextPaint textPaint = new TextPaint();
        textPaint.linkColor = Color.GRAY;
        underlineSpan.updateDrawState(textPaint);


        if (copyChecked.get(position)) {
            holder.m_content.setTypeface(null, Typeface.BOLD);
//            SpannableString spannableString = new SpannableString(content.get(position));
//            spannableString.setSpan(underlineSpan, 0, spannableString.length(), 0);
//            holder.m_content.setText(spannableString);
            holder.m_content.setText(Html.fromHtml("<u><FONT COLOR=\"#505050\" >"+content.get(position)+"</Font></u>"));
        } else {
            holder.m_content.setTypeface(null, Typeface.NORMAL);
            holder.m_content.setText(content.get(position));
        }

        holder.m_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (copyChecked.get(position)) {
                    copyChecked.set(position, false);
                    holder.m_content.setTypeface(null, Typeface.NORMAL);
                    holder.m_content.setText(content.get(position));
                } else {
                    copyChecked.set(position, true);
                    holder.m_content.setTypeface(null, Typeface.BOLD);
//                    SpannableString spannableString = new SpannableString(content.get(position));
//                    spannableString.setSpan(underlineSpan, 0, spannableString.length(), 0);
//                    holder.m_content.setText(spannableString);
                    holder.m_content.setText(Html.fromHtml("<u><FONT COLOR=\"#505050\" >"+content.get(position)+"</Font></u>"));
                }
            }
        });

//        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                float textSize = holder.m_content.getTextSize();
//                if(intent.getBooleanExtra("bigger", false)){
//                    holder.m_content.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize + 6);
//                } else {
//                    holder.m_content.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize - 6);
//                }
//                notifyItemChanged(position);
//            }
//        };
//        IntentFilter intFilt = new IntentFilter("BIBLE_TEXTSIZE");
//        holder.itemView.getContext().registerReceiver(broadcastReceiver, intFilt);

    }

    @Override
    public int getItemCount() {
        return content.size();
    }


    public void copyBible() {
        Log.w(TAG, "copyBible");

        // 체크된 verse가 있는지 없는지 확인
        boolean isChecked = false;
        for(boolean verseChecked : copyChecked){
            if(verseChecked)
                isChecked = true;
        }

        if(isChecked) {

            StringBuilder stringBuilder = new StringBuilder();

            // 성경 이름 구절 스트링
            stringBuilder.append(tabTitle);
            stringBuilder.append(" ");

            int startIndex = -1;

            for (int k = 0; k < copyChecked.size(); k++) {
                if (copyChecked.get(k)) {
                    if (startIndex == -1) {
                        startIndex = k;
                        stringBuilder.append(k + init);
                    }
                } else {
                    if (startIndex != -1) {
                        if (startIndex == k - 1) {
                            stringBuilder.append(",");
                        } else {
                            stringBuilder.append("~");
                            stringBuilder.append(k - 1 + init);
                            stringBuilder.append(",");
                        }
                    }

                    startIndex = -1;
                }
            }

            if (startIndex != -1) {
                if (startIndex != copyChecked.size() - 1) {
                    stringBuilder.append("~");
                    stringBuilder.append(copyChecked.size() - 1 + init);
                }
            } else {
                stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            }

            stringBuilder.append("절\n");


            // 말씀 구절 복사
            for (int j = 0; j < copyChecked.size(); j++) {
                if (copyChecked.get(j)) {
                    stringBuilder.append(j + init);
                    stringBuilder.append(" ");
                    stringBuilder.append(content.get(j));
                    stringBuilder.append("\n");
                }
            }

            ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("BTJ Prayer", stringBuilder);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(mContext, "복사되었습니다", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "복사할 말씀 구절을 선택하고 복사하기 버튼을 눌러주세요", Toast.LENGTH_SHORT).show();
        }
    }


}
