package com.btjcenter.pray4m;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.btjcenter.pray4m.activity.PostActivity;
import com.btjcenter.pray4m.thread.GetHome;
import com.btjcenter.pray4m.thread.GetNewNations;
import com.btjcenter.pray4m.thread.GetPostList;
import com.btjcenter.pray4m.utils.DateProvider;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by alexmoon on 2015. 11. 13..
 */
public class HomePageholderFragment extends Fragment{

    int page_num;

    public static HomePageholderFragment create(int p_num){
        HomePageholderFragment fragment = new HomePageholderFragment();
        Bundle args = new Bundle();
        args.putInt("page", p_num);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page_num = getArguments().getInt("page");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootView = null;

        switch(page_num){
            case 0:
                rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_home, container, false);
                new GetHome(rootView).execute();
                break;
            case 1:
                rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_today, container, false);
                new GetPostList("today", rootView, R.id.postlist_today).execute();
                break;
            case 2:
                rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_nations, container, false);
                final View rv = rootView;
                ImageView nation1 = (ImageView)rootView.findViewById(R.id.nations_button1);
                nation1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8804);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation2 = (ImageView)rootView.findViewById(R.id.nations_button2);
                nation2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8812);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation3 = (ImageView)rootView.findViewById(R.id.nations_button3);
                nation3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8810);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation4 = (ImageView)rootView.findViewById(R.id.nations_button4);
                nation4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8808);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation5 = (ImageView)rootView.findViewById(R.id.nations_button5);
                nation5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8802);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation6 = (ImageView)rootView.findViewById(R.id.nations_button6);
                nation6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8614);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation7 = (ImageView)rootView.findViewById(R.id.nations_button7);
                nation7.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8814);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation8 = (ImageView)rootView.findViewById(R.id.nations_button8);
                nation8.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8806);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation9 = (ImageView)rootView.findViewById(R.id.nations_button9);
                nation9.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8618);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation10 = (ImageView)rootView.findViewById(R.id.nations_button10);
                nation10.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8824);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation11 = (ImageView)rootView.findViewById(R.id.nations_button11);
                nation11.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8828);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation12 = (ImageView)rootView.findViewById(R.id.nations_button12);
                nation12.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8832);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation13 = (ImageView)rootView.findViewById(R.id.nations_button13);
                nation13.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8830);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation14 = (ImageView)rootView.findViewById(R.id.nations_button14);
                nation14.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8834);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation15 = (ImageView)rootView.findViewById(R.id.nations_button15);
                nation15.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8816);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation16 = (ImageView)rootView.findViewById(R.id.nations_button16);
                nation16.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8762);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation17 = (ImageView)rootView.findViewById(R.id.nations_button17);
                nation17.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8758);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation18 = (ImageView)rootView.findViewById(R.id.nations_button18);
                nation18.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8753);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation19 = (ImageView)rootView.findViewById(R.id.nations_button19);
                nation19.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8760);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation20 = (ImageView)rootView.findViewById(R.id.nations_button20);
                nation20.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8790);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation21 = (ImageView)rootView.findViewById(R.id.nations_button21);
                nation21.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8792);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation22 = (ImageView)rootView.findViewById(R.id.nations_button22);
                nation22.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8784);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation23 = (ImageView)rootView.findViewById(R.id.nations_button23);
                nation23.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8786);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation24 = (ImageView)rootView.findViewById(R.id.nations_button24);
                nation24.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8788);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation25 = (ImageView)rootView.findViewById(R.id.nations_button25);
                nation25.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8780);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation26 = (ImageView)rootView.findViewById(R.id.nations_button26);
                nation26.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8772);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation27 = (ImageView)rootView.findViewById(R.id.nations_button27);
                nation27.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8775);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation28 = (ImageView)rootView.findViewById(R.id.nations_button28);
                nation28.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8777);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation29 = (ImageView)rootView.findViewById(R.id.nations_button29);
                nation29.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8770);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation30 = (ImageView)rootView.findViewById(R.id.nations_button30);
                nation30.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8765);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation31 = (ImageView)rootView.findViewById(R.id.nations_button31);
                nation31.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8818);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation32 = (ImageView)rootView.findViewById(R.id.nations_button32);
                nation32.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8820);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation33 = (ImageView)rootView.findViewById(R.id.nations_button33);
                nation33.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8822);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation34 = (ImageView)rootView.findViewById(R.id.nations_button34);
                nation34.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8794);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation35 = (ImageView)rootView.findViewById(R.id.nations_button35);
                nation35.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8796);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation36 = (ImageView)rootView.findViewById(R.id.nations_button36);
                nation36.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8798);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation37 = (ImageView)rootView.findViewById(R.id.nations_button37);
                nation37.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8800);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nation38 = (ImageView)rootView.findViewById(R.id.nations_button38);
                nation38.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 8836);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });
                ImageView nationGeorgia = (ImageView)rootView.findViewById(R.id.nations_button_georgia);
                nationGeorgia.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(rv.getContext(), PostActivity.class);
                        intent.putExtra("id", 18550);
                        intent.putExtra("nation", true);
                        rv.getContext().startActivity(intent);
                    }
                });

                new GetNewNations(rv).execute();

                break;
//            case 3:
//                rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_best, container, false);
//                new GetHotPostList(rootView).execute();
//                break;
            case 3:
                rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_pds, container, false);
                new GetPostList("columns", rootView, R.id.postlist_pds).execute();
                break;
            case 4:
                rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_prev, container, false);
                final ViewGroup rv4 = rootView;
                RelativeLayout layout_ga, layout_nations, layout_today;
                final LinearLayout layout_cat, child_ga, child_nations, child_today, layout_recycler;
                final ImageView arrow_ga, arrow_nations, arrow_today, recycler_return;
                final TextView text_14ramadan, text_14ramadan_eng, text_15ramadan, text_15ramadan_eng,
                        text_16ramadan, text_16ramadan_eng, text_16ramadan_cn, text_16ramadan_jap,
                        text_40d, text_40d_eng, text_40d_cn, text_nation, text_today_eng,
                        text_list_cat, text_list_title;
                layout_ga = (RelativeLayout) rootView.findViewById(R.id.layout_prev_ga);
                layout_nations = (RelativeLayout) rootView.findViewById(R.id.layout_prev_nations);
                layout_today = (RelativeLayout) rootView.findViewById(R.id.layout_prev_today);
                layout_cat = (LinearLayout) rootView.findViewById(R.id.layout_prev_categories);
                child_ga = (LinearLayout) rootView.findViewById(R.id.layout_prev_ga_child);
                child_nations = (LinearLayout) rootView.findViewById(R.id.layout_prev_nations_child);
                child_today = (LinearLayout) rootView.findViewById(R.id.layout_prev_today_child);
                layout_recycler = (LinearLayout) rootView.findViewById(R.id.layout_prev_list);
                arrow_ga = (ImageView) rootView.findViewById(R.id.button_prev_ga_arrow);
                arrow_nations = (ImageView) rootView.findViewById(R.id.button_prev_nation_arrow);
                arrow_today = (ImageView) rootView.findViewById(R.id.button_prev_today_arrow);
                recycler_return = (ImageView) rootView.findViewById(R.id.image_prev_list_return);
                text_14ramadan = (TextView) rootView.findViewById(R.id.text_prev_14ramadan);
                text_14ramadan_eng = (TextView) rootView.findViewById(R.id.text_prev_14ramadan_eng);
                text_15ramadan = (TextView) rootView.findViewById(R.id.text_prev_15ramadan);
                text_15ramadan_eng = (TextView) rootView.findViewById(R.id.text_prev_15ramadan_eng);
                text_16ramadan = (TextView) rootView.findViewById(R.id.text_prev_16ramadan);
                text_16ramadan_eng = (TextView) rootView.findViewById(R.id.text_prev_16ramadan_eng);
                text_16ramadan_cn = (TextView) rootView.findViewById(R.id.text_prev_16ramadan_cn);
                text_16ramadan_jap = (TextView) rootView.findViewById(R.id.text_prev_16ramadan_jap);

                text_40d = (TextView) rootView.findViewById(R.id.text_prev_40d);
                text_40d_eng = (TextView) rootView.findViewById(R.id.text_prev_40d_eng);
                text_40d_cn = (TextView) rootView.findViewById(R.id.text_prev_40d_cn);
                text_nation = (TextView) rootView.findViewById(R.id.text_prev_nation);
                text_today_eng = (TextView) rootView.findViewById(R.id.text_prev_today_eng);
                text_list_cat = (TextView) rootView.findViewById(R.id.textview_prev_list_category);
                text_list_title = (TextView) rootView.findViewById(R.id.textview_prev_list_title);

                Calendar current = Calendar.getInstance();
                if(current.after(new DateProvider().RAMADAN16_END)){
                    text_16ramadan.setVisibility(View.VISIBLE);
                    text_16ramadan_eng.setVisibility(View.VISIBLE);
                    text_16ramadan_cn.setVisibility(View.VISIBLE);
                    text_16ramadan_jap.setVisibility(View.VISIBLE);
                }



                layout_ga.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(child_ga.getVisibility() == View.GONE) {
                            child_ga.setVisibility(View.VISIBLE);
                            arrow_ga.setRotation(180);
                        } else {
                            child_ga.setVisibility(View.GONE);
                            arrow_ga.setRotation(0);
                        }
                    }
                });
                layout_nations.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(child_nations.getVisibility() == View.GONE) {
                            child_nations.setVisibility(View.VISIBLE);
                            arrow_nations.setRotation(180);
                        } else {
                            child_nations.setVisibility(View.GONE);
                            arrow_nations.setRotation(0);
                        }
                    }
                });
                layout_today.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(child_today.getVisibility() == View.GONE) {
                            child_today.setVisibility(View.VISIBLE);
                            arrow_today.setRotation(180);
                        } else {
                            child_today.setVisibility(View.GONE);
                            arrow_today.setRotation(0);
                        }
                    }
                });


                recycler_return.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.VISIBLE);
                        layout_recycler.setVisibility(View.GONE);
                    }
                });


                text_14ramadan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("2014 라마단 무슬림을 살리는 30일 기도운동");
                        new GetPostList("14p4m", rv4, R.id.recycler_prev).execute();
                    }
                });
                text_14ramadan_eng.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("2014 라마단 무슬림을 살리는 30일 기도운동");
                        new GetPostList("14p4meng", rv4, R.id.recycler_prev).execute();
                    }
                });
                text_15ramadan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("2015 라마단 무슬림을 살리는 30일 기도운동");
                        new GetPostList("15p4m", rv4, R.id.recycler_prev).execute();
                    }
                });
                text_15ramadan_eng.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("2015 라마단 무슬림을 살리는 30일 기도운동");
                        new GetPostList("15p4meng", rv4, R.id.recycler_prev).execute();
                    }
                });
                text_16ramadan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("2016 라마단 무슬림을 살리는 30일 기도운동");
                        new GetPostList("16p4m", rv4, R.id.recycler_prev).execute();
                    }
                });
                text_16ramadan_eng.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("2016 라마단 무슬림을 살리는 30일 기도운동");
                        new GetPostList("16p4meng", rv4, R.id.recycler_prev).execute();
                    }
                });
                text_16ramadan_cn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("2016 라마단 무슬림을 살리는 30일 기도운동");
                        new GetPostList("16p4mcn", rv4, R.id.recycler_prev).execute();
                    }
                });
                text_16ramadan_jap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("2016 라마단 무슬림을 살리는 30일 기도운동");
                        new GetPostList("16p4mcn", rv4, R.id.recycler_prev).execute();
                    }
                });

                text_40d.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("지구적 영적 전쟁을 위한 40일 금식기도");
                        new GetPostList("40daysko", rv4, R.id.recycler_prev).execute();
                    }
                });

                text_40d_eng.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("지구적 영적 전쟁을 위한 40일 금식기도");
                        new GetPostList("40dayseng", rv4, R.id.recycler_prev).execute();
                    }
                });
                text_40d_cn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("GA 기도운동");
                        text_list_title.setText("지구적 영적 전쟁을 위한 40일 금식기도");
                        new GetPostList("40dayschi", rv4, R.id.recycler_prev).execute();
                    }
                });
                text_nation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("종족기도");
                        text_list_title.setText("이전 종족기도");
                        new GetPostList("nations", rv4, R.id.recycler_prev).execute();
                    }
                });
                text_today_eng.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout_cat.setVisibility(View.GONE);
                        layout_recycler.setVisibility(View.VISIBLE);
                        text_list_cat.setText("오늘의 기도");
                        text_list_title.setText("Today's Prayer");
                        new GetPostList("todays-english", rv4, R.id.recycler_prev).execute();
                    }
                });


                break;
        }
        return rootView;
    }


}
