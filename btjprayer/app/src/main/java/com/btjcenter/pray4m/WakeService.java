package com.btjcenter.pray4m;

import android.app.IntentService;
import android.content.Intent;

import com.btjcenter.pray4m.activity.WakeActivity;
import com.btjcenter.pray4m.db.TinyDB;

import java.util.ArrayList;
import java.util.Calendar;

public class WakeService extends IntentService {
    public WakeService() {
        super("WakeService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int index = intent.getIntExtra("index", -1);
        TinyDB tinyDB = new TinyDB(getApplicationContext());
        ArrayList<String> alarms = tinyDB.getListString("alarms");

        String[] alarminfo = alarms.get(index).split(";");
        boolean enabled = Boolean.parseBoolean(alarminfo[0]);
        int hour = Integer.parseInt(alarminfo[1]);
        int min = Integer.parseInt(alarminfo[2]);
        boolean mon = Boolean.parseBoolean(alarminfo[3]);
        boolean tue = Boolean.parseBoolean(alarminfo[4]);
        boolean wed = Boolean.parseBoolean(alarminfo[5]);
        boolean thu = Boolean.parseBoolean(alarminfo[6]);
        boolean fri = Boolean.parseBoolean(alarminfo[7]);
        boolean sat = Boolean.parseBoolean(alarminfo[8]);
        boolean sun = Boolean.parseBoolean(alarminfo[9]);
        boolean repeating = mon||tue||wed||thu||fri||sat||sun;
        String memo = "";
        if(alarminfo.length == 11)
            memo = alarminfo[10];
        String ampm = "am";
        String time = "";
        if(hour > 11)
            ampm = "pm";
        if(hour < 13){
            time += hour + ":";
        } else {
            time += (hour - 12) + ":";
        }
        if(min < 10)
            time += "0" + min + ampm;
        else
            time += "" + min + ampm;


        Intent wake = new Intent(this, WakeActivity.class);
        wake.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        wake.putExtra("memo", memo);
        wake.putExtra("index", index);
        wake.putExtra("time", time);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        if(repeating){
            switch (calendar.get(Calendar.DAY_OF_WEEK)){
                case 1:
                    if(sun)
                        startActivity(wake);break;
                case 2:
                    if(mon)
                        startActivity(wake);break;
                case 3:
                    if(tue)
                        startActivity(wake);break;
                case 4:
                    if(wed)
                        startActivity(wake);break;
                case 5:
                    if(thu)
                        startActivity(wake);break;
                case 6:
                    if(fri)
                        startActivity(wake);break;
                case 7:
                    if(sat)
                        startActivity(wake);break;
            }
        } else {
            String newAlarmInfo =  "false;" + alarminfo[1]
                    + ";" + alarminfo[2]
                    + ";" + alarminfo[3]
                    + ";" + alarminfo[4]
                    + ";" + alarminfo[5]
                    + ";" + alarminfo[6]
                    + ";" + alarminfo[7]
                    + ";" + alarminfo[8]
                    + ";" + alarminfo[9]
                    + ";";
            if(alarminfo.length == 11)
                newAlarmInfo += alarminfo[10];
            if(index != -1)
                alarms.set(index, newAlarmInfo);
            tinyDB.putListString("alarms", alarms);

            startActivity(wake);
        }


        AlarmReceiver.completeWakefulIntent(intent);
    }
}
