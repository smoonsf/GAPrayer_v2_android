package com.btjcenter.pray4m;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.btjcenter.pray4m.adapter.BibleListAdapter;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by alexmoon on 2015. 11. 13..
 */
public class BiblePageholderFragment extends Fragment {
    private static final String TAG = "BiblePageholderFragment";

    int init;
    String tabTitle;
    ArrayList<String> verse;
    boolean gap_table = false;
    private int tableIndex = 1;
    private int[] tableResIds = {
            R.drawable.gap_table1,
            R.drawable.gap_table2,
            R.drawable.gap_table3,
            R.drawable.gap_table4,
            R.drawable.gap_table5,
            R.drawable.gap_table6,
            R.drawable.gap_table7,
            R.drawable.gap_table8,
            R.drawable.gap_table9,
            R.drawable.gap_table10,
            R.drawable.gap_table11,
            R.drawable.gap_table12,
    };

    BibleListAdapter bibleListAdapter;


    public static BiblePageholderFragment create() {
        BiblePageholderFragment fragment = new BiblePageholderFragment();
        Bundle args = new Bundle();
        args.putBoolean("gaptable", true);
        fragment.setArguments(args);
        return fragment;
    }

    public static BiblePageholderFragment create(int _init, ArrayList<String> _verses, String tabTitle) {
        BiblePageholderFragment fragment = new BiblePageholderFragment();
        Bundle args = new Bundle();
        args.putInt("init", _init);
        args.putStringArrayList("verses", _verses);
        args.putString("tabTitle", tabTitle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init = getArguments().getInt("init");
        verse = getArguments().getStringArrayList("verses");
        gap_table = getArguments().getBoolean("gaptable", false);
        tabTitle = getArguments().getString("tabTitle", "");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = null;

        if (gap_table) {
            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_gaptable, container, false);
            final ImageView table = (ImageView) rootView.findViewById(R.id.image_table);

            tableIndex = Calendar.getInstance().get(Calendar.MONTH);
            table.setImageResource(tableResIds[tableIndex]);


            ImageView left = (ImageView) rootView.findViewById(R.id.image_left);
            ImageView right = (ImageView) rootView.findViewById(R.id.image_right);

            left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tableIndex -= 1;
                    if (tableIndex < 0)
                        tableIndex = 11;

                    table.setImageResource(tableResIds[tableIndex]);
                }
            });

            right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tableIndex += 1;
                    if (tableIndex > 11)
                        tableIndex = 0;

                    table.setImageResource(tableResIds[tableIndex]);
                }
            });


        } else {
            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_bible, container, false);
            RecyclerView biblelist = (RecyclerView) rootView.findViewById(R.id.recycler_bible);
            LinearLayoutManager layoutManager = new LinearLayoutManager(rootView.getContext());
            biblelist.setLayoutManager(layoutManager);
            bibleListAdapter = new BibleListAdapter(getActivity(), init, verse, tabTitle);
            biblelist.setAdapter(bibleListAdapter);


        }


        return rootView;
    }

    public void copyBible() {
        Log.w(TAG, "copyBible");
        Log.w(TAG, "gap_table : " + gap_table + " //adapter null" + (bibleListAdapter == null));
        if (bibleListAdapter != null && !gap_table) {
            bibleListAdapter.copyBible();
        }
    }

}
