package com.btjcenter.pray4m.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.btjcenter.pray4m.AlarmReceiver;
import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.db.TinyDB;

import java.util.ArrayList;

public class SetAlarmActivity extends BTJPrayerBaseActivity {

    final static public String TAG = "SetAlarmActivity";

    int index;
    ArrayList<String> alarms;
    String alarminfo;

    TimePicker timePicker;
    ToggleButton toggleButton_mon;
    ToggleButton toggleButton_tue;
    ToggleButton toggleButton_wed;
    ToggleButton toggleButton_thu;
    ToggleButton toggleButton_fri;
    ToggleButton toggleButton_sat;
    ToggleButton toggleButton_sun;
    EditText editText_memo;
    Button button_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("알람 시간 설정");
        setSupportActionBar(toolbar);

        index = getIntent().getIntExtra("index", -1);

        timePicker = (TimePicker)findViewById(R.id.timepicker_setalarm);
        toggleButton_mon = (ToggleButton)findViewById(R.id.toggle_setalarm_mon);
        toggleButton_tue = (ToggleButton)findViewById(R.id.toggle_setalarm_tue);
        toggleButton_wed = (ToggleButton)findViewById(R.id.toggle_setalarm_wed);
        toggleButton_thu = (ToggleButton)findViewById(R.id.toggle_setalarm_thu);
        toggleButton_fri = (ToggleButton)findViewById(R.id.toggle_setalarm_fri);
        toggleButton_sat = (ToggleButton)findViewById(R.id.toggle_setalarm_sat);
        toggleButton_sun = (ToggleButton)findViewById(R.id.toggle_setalarm_sun);
        editText_memo = (EditText)findViewById(R.id.edittext_memo_setalarm);
        button_ok = (Button)findViewById(R.id.button_ok_setalarm);

        TinyDB tinyDB = new TinyDB(getApplicationContext());
        alarms = tinyDB.getListString("alarms");

        String enabled = "true";

        if(index != -1){
            alarminfo = alarms.get(index);
            String[] infoArray = alarminfo.split(";");
            enabled = infoArray[0];
            timePicker.setCurrentHour(Integer.parseInt(infoArray[1]));
            timePicker.setCurrentMinute(Integer.parseInt(infoArray[2]));
            if(Boolean.parseBoolean(infoArray[3]))
                toggleButton_mon.setChecked(true);
            if(Boolean.parseBoolean(infoArray[4]))
                toggleButton_tue.setChecked(true);
            if(Boolean.parseBoolean(infoArray[5]))
                toggleButton_wed.setChecked(true);
            if(Boolean.parseBoolean(infoArray[6]))
                toggleButton_thu.setChecked(true);
            if(Boolean.parseBoolean(infoArray[7]))
                toggleButton_fri.setChecked(true);
            if(Boolean.parseBoolean(infoArray[8]))
                toggleButton_sat.setChecked(true);
            if(Boolean.parseBoolean(infoArray[9]))
                toggleButton_sun.setChecked(true);
            if(infoArray.length == 11)
                editText_memo.setText(infoArray[10]);
        }


        //final AlarmReceiver alarmReceiver = new AlarmReceiver();
        final Context context = this;
        final String finalEnabled = enabled;
        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newAlarmInfo = "true;" + timePicker.getCurrentHour() + ";" +
                        timePicker.getCurrentMinute() + ";" +
                        toggleButton_mon.isChecked() + ";" +
                        toggleButton_tue.isChecked() + ";" +
                        toggleButton_wed.isChecked() + ";" +
                        toggleButton_thu.isChecked() + ";" +
                        toggleButton_fri.isChecked() + ";" +
                        toggleButton_sat.isChecked() + ";" +
                        toggleButton_sun.isChecked() + ";" +
                        editText_memo.getText();
                Log.d(TAG, newAlarmInfo);
                if(index == -1){
                    alarms.add(newAlarmInfo);
                    TinyDB tinyDB = new TinyDB(getApplicationContext());
                    tinyDB.clear();
                    tinyDB.putListString("alarms", alarms);
                } else {
                    alarms.set(index, newAlarmInfo);
                    TinyDB tinyDB = new TinyDB(getApplicationContext());
                    tinyDB.clear();
                    tinyDB.putListString("alarms", alarms);
                }

                ArrayList<Boolean> delete_list = new ArrayList<Boolean>();
                for(int i=0;i<alarms.size();i++)
                    delete_list.add(false);
                TinyDB tinyDB = new TinyDB(getApplicationContext());
                tinyDB.putListBoolean("delete_list", delete_list);

                AlarmReceiver alarmReceiver = new AlarmReceiver();
                alarmReceiver.cancelAlarm(v.getContext().getApplicationContext());
                alarmReceiver.setAlarm(v.getContext().getApplicationContext());
                finish();
            }
        });







    }

}
