package com.btjcenter.pray4m.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.btjcenter.pray4m.AlarmReceiver;
import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.db.MySQLiteOpenHelper;
import com.btjcenter.pray4m.db.TinyDB;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SplashActivity extends BTJPrayerBaseActivity {

    private static final String TAG = "SplashActivity";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    Context context;
    TextView mDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;

        mDisplay = (TextView) findViewById(R.id.test);

        if(!isNetworkAvailable())
            this.finish();


        TinyDB tinyDB = new TinyDB(getApplicationContext());
        if (tinyDB.getListString("alarms").size() == 0) {
            AlarmReceiver alarmReceiver = new AlarmReceiver();
            alarmReceiver.cancelAlarm(context.getApplicationContext());
        }

        try {
            boolean bResult = isCheckDB(context);  // DB가 있는지?
            MySQLiteOpenHelper helper = new MySQLiteOpenHelper(this);
            Log.d("MiniApp", "DB Check=" + bResult);
            if (!bResult) {   // DB가 없으면 복사
                copyDB(context);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, 2000);
            } else {
                Log.d(TAG, "DB exists");
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, 1500);
            }
        } catch (Exception e) {

        }

        Log.i(TAG, "Device Token : " + FirebaseInstanceId.getInstance().getToken());


    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }



    private boolean checkPlayServices() {
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GoogleApiAvailability.getInstance().isUserResolvableError(resultCode)) {
                GoogleApiAvailability.getInstance().getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }



//    private SharedPreferences getGCMPreferences(Context context) {
//        return getSharedPreferences(SplashActivity.class.getSimpleName(), Context.MODE_PRIVATE);
//    }
//
//    private static int getAppVersion(Context context) {
//        try {
//            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
//            return packageInfo.versionCode;
//        } catch (PackageManager.NameNotFoundException e) {
//            throw new RuntimeException("Could not get package name: " + e);
//        }
//    }
//
//
//    private String capitalize(String s) {
//        if (s == null || s.length() == 0) {
//            return "";
//        }
//        char first = s.charAt(0);
//        if (Character.isUpperCase(first)) {
//            return s;
//        } else {
//            return Character.toUpperCase(first) + s.substring(1);
//        }
//    }


    // DB가 있나 체크하기
    public boolean isCheckDB(Context mContext) {
        String filePath = "/data/data/" + context.getPackageName() + "/databases/gap5.db";
        File file = new File(filePath);

        if (file.exists()) {
            return true;
        }

        String oldFilePath1 = "/data/data/" + context.getPackageName() + "/databases/gap1.db";
        File olddb1 = new File(oldFilePath1);

        if (olddb1.exists()) {
            olddb1.delete();
        }

        String oldFilePath2 = "/data/data/" + context.getPackageName() + "/databases/gap2.db";
        File olddb2 = new File(oldFilePath2);

        if (olddb2.exists()) {
            olddb2.delete();
        }

        String oldFilePath3 = "/data/data/" + context.getPackageName() + "/databases/gap3.db";
        File olddb3 = new File(oldFilePath3);

        if (olddb3.exists()) {
            olddb3.delete();
        }

        String oldFilePath4 = "/data/data/" + context.getPackageName() + "/databases/gap4.db";
        File olddb4 = new File(oldFilePath4);

        if (olddb4.exists())    {
            olddb4.delete();
        }

        return false;

    }

    // DB를 복사하기
// assets의 /db/xxxx.db 파일을 설치된 프로그램의 내부 DB공간으로 복사하기
    public void copyDB(Context mContext) {
        Log.d(TAG, "copyDB");
        AssetManager manager = mContext.getAssets();
        String folderPath = "/data/data/" + mContext.getPackageName() + "/databases";
        String filePath = "/data/data/" + mContext.getPackageName() + "/databases/gap5.db";
        File folder = new File(folderPath);
        File file = new File(filePath);

        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            InputStream is = manager.open("db/gap5.db");
            BufferedInputStream bis = new BufferedInputStream(is);

            if (folder.exists()) {
            } else {
                folder.mkdirs();
            }


            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }

            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            int read = -1;
            byte[] buffer = new byte[1024];
            while ((read = bis.read(buffer, 0, 1024)) != -1) {
                bos.write(buffer, 0, read);
            }

            bos.flush();

            bos.close();
            fos.close();
            bis.close();
            is.close();

        } catch (IOException e) {
            Log.e("ErrorMessage : ", e.getMessage());
        }

    }

    public boolean isNetworkAvailable()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {	return true;
        }
        Toast.makeText(this, "인터넷 연결이 되어있지 않습니다. 네트워크 연결을 확인해주세요.", Toast.LENGTH_LONG).show();
        return false;
    }
}
