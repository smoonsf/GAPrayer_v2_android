package com.btjcenter.pray4m.api;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by alexmoon on 2015. 11. 9..
 */
public class ListDeserializer<T> implements JsonDeserializer<T> {

    Class<T> mClass;
    String object;

    public ListDeserializer(Class<T> targetClass, String obj){
        object = obj;
        mClass = targetClass;
    }

    @Override
    public T deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException
    {
        JsonElement objects = je.getAsJsonObject().get(object);
        return new Gson().fromJson(objects,type);
    }
}