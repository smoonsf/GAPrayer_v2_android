package com.btjcenter.pray4m.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.api.PostService;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.UUID;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class GapJoinActivity extends BTJPrayerBaseActivity {

    static public final String TAG = "GapJoinActivity";

    EditText editText_name;
    RadioGroup radioGroup_sex;
    Spinner spinner_age;
    Spinner spinner_area;
    Button button_submit;
    Button button_alarm;

    String sex;
    String age;
    String country;
    String ip_addr;

    ArrayList<String> countryList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gapjoin);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.97);
        getWindow().setLayout(screenWidth, WindowManager.LayoutParams.MATCH_PARENT);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = sharedPreferences.edit();
//        if(sharedPreferences.getBoolean("joined", false)){
//            Toast.makeText(this, "이미 참여하셨습니다", Toast.LENGTH_SHORT).show();
//            finish();
//        }

        final ArrayList<String> ageList = new ArrayList<String>();
        countryList = new ArrayList<String>();

        editText_name = (EditText) findViewById(R.id.edittext_name_gapjoin);

        radioGroup_sex = (RadioGroup) findViewById(R.id.radiogroup_sex_gapjoin);
        sex = "";
        radioGroup_sex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radiobutton_man_gapjoin:
                        sex = "남성";
                        break;
                    case R.id.radiobutton_woman_gapjoin:
                        sex = "여성";
                        break;
                }
            }
        });

        ageList.add("10대");
        ageList.add("20대");
        ageList.add("30대");
        ageList.add("40대");
        ageList.add("50대");
        ageList.add("60대 이상");
        ArrayAdapter<String> ageAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, ageList);

        spinner_age = (Spinner) findViewById(R.id.spinner_age_gapjoin);
        spinner_age.setAdapter(ageAdapter);
        age = "";
        spinner_age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                age = ageList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        addCountry();
        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, countryList);
        spinner_area = (Spinner) findViewById(R.id.spinner_area_gapjoin);
        spinner_area.setAdapter(countryAdapter);
        country = "";
        spinner_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country = countryList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ip_addr = getIpAddress();

        button_submit = (Button) findViewById(R.id.button_submit_gapjoin);
        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder()
                        .validateEagerly()
                        .baseUrl("http://www.btjprayer.net")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                PostService postService = retrofit.create(PostService.class);
                if(editText_name.getText().length() == 0){
                    Toast.makeText(GapJoinActivity.this, "이름을 입력해주세요", Toast.LENGTH_SHORT).show();
                } else if (sex.length() == 0) {
                    Toast.makeText(GapJoinActivity.this, "성별을 입력해주세요", Toast.LENGTH_SHORT).show();
                } else if (age.length() == 0) {
                    Toast.makeText(GapJoinActivity.this, "나이를 입력해주세요", Toast.LENGTH_SHORT).show();
                } else if (country.length() == 0) {
                    Toast.makeText(GapJoinActivity.this, "거주 국가를 선택해주세요", Toast.LENGTH_SHORT).show();
                } else {
                    Call<String> gapJoinCall = postService.sendGapJoin(getDeviceId(),
                            editText_name.getText().toString(), age, sex, country, ip_addr);
                    gapJoinCall.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Response<String> response, Retrofit retrofit) {
                            //Log.d(TAG, "Gap Join Sended");
                        }

                        @Override
                        public void onFailure(Throwable throwable) {

                        }
                    });
                    Toast.makeText(v.getContext(), "할렐루야~! GAP 기도운동가가 되셨습니다^^", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("joined", true);
                    editor.apply();
                    finish();
                }

            }
        });

        button_alarm = (Button) findViewById(R.id.button_alarm_gapjoin);
        button_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AlarmActivity.class);
                startActivity(intent);
            }
        });
    }

    private String getDeviceId() {
        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();

        return deviceId;
    }

    private String getIpAddress() {
        ConnectivityManager connMgr = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isAvailable()) {
            WifiManager myWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
            WifiInfo myWifiInfo = myWifiManager.getConnectionInfo();
            int ipAddress = myWifiInfo.getIpAddress();

            System.out.println("WiFi address is " + android.text.format.Formatter.formatIpAddress(ipAddress));
            return android.text.format.Formatter.formatIpAddress(ipAddress);
        } else if (mobile.isAvailable()) {
            return GetLocalIpAddress();
        } else {
            return "Network Unavailable";
        }
    }

    private String GetLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException ex ) {
            return "ERROR Obtaining IP";
        }

        return "No IP Available";
    }

    private void addCountry(){
        countryList.add("대한민국");
        countryList.add("가나");
        countryList.add("가봉");
        countryList.add("가이아나");
        countryList.add("감비아");
        countryList.add("과테말라");
        countryList.add("그레나다");
        countryList.add("그리스");
        countryList.add("기니");
        countryList.add("기니비사우");
        countryList.add("나미비아");
        countryList.add("나우루");
        countryList.add("나이지리아");
        countryList.add("남수단");
        countryList.add("남아프리카 공화국");
        countryList.add("남오세티야");
        countryList.add("네덜란드");
        countryList.add("네팔");
        countryList.add("노르웨이");
        countryList.add("뉴질랜드");
        countryList.add("니제르");
        countryList.add("니카라과");
        countryList.add("대만");
        countryList.add("덴마크");
        countryList.add("도미니카 공화국");
        countryList.add("도미니카 연방");
        countryList.add("독일");
        countryList.add("동티모르");
        countryList.add("라오스");
        countryList.add("라이베리아");
        countryList.add("라트비아");
        countryList.add("러시아");
        countryList.add("레바논");
        countryList.add("레소토");
        countryList.add("루마니아");
        countryList.add("룩셈부르크");
        countryList.add("르완다");
        countryList.add("리비아");
        countryList.add("리투아니아");
        countryList.add("리히텐슈타인");
        countryList.add("마다가스카르");
        countryList.add("마셜 제도");
        countryList.add("마케도니아");
        countryList.add("말라위");
        countryList.add("말레이시아");
        countryList.add("말리");
        countryList.add("멕시코");
        countryList.add("모나코");
        countryList.add("모로코");
        countryList.add("모리셔스");
        countryList.add("모리타니");
        countryList.add("모잠비크");
        countryList.add("몬테네그로");
        countryList.add("몰도바");
        countryList.add("몰디브");
        countryList.add("몰타");
        countryList.add("몽골");
        countryList.add("미국");
        countryList.add("미얀마");
        countryList.add("미크로네시아");
        countryList.add("바누아투");
        countryList.add("바레인");
        countryList.add("바베이도스");
        countryList.add("바티칸시국");
        countryList.add("바하마");
        countryList.add("방글라데시");
        countryList.add("베냉");
        countryList.add("베네수엘라");
        countryList.add("베트남");
        countryList.add("벨기에");
        countryList.add("벨라루스");
        countryList.add("벨리즈");
        countryList.add("보스니아 헤르체고비나");
        countryList.add("보츠와나");
        countryList.add("볼리비아");
        countryList.add("부룬디");
        countryList.add("부르키나파소");
        countryList.add("부탄(Bhutan)");
        countryList.add("북키프로스 튀르크 공화국");
        countryList.add("불가리아");
        countryList.add("브라질");
        countryList.add("브루나이");
        countryList.add("사모아");
        countryList.add("사우디아라비아");
        countryList.add("산마리노");
        countryList.add("상투메프린시페");
        countryList.add("세네갈");
        countryList.add("세르비아");
        countryList.add("세이셸");
        countryList.add("세인트 루시아");
        countryList.add("세인트 빈센트 그레나딘");
        countryList.add("세인트 키츠 네비스");
        countryList.add("소말리아");
        countryList.add("솔로몬 제도");
        countryList.add("수단");
        countryList.add("수리남");
        countryList.add("스리랑카");
        countryList.add("스와질란드");
        countryList.add("스웨덴");
        countryList.add("스위스");
        countryList.add("스페인");
        countryList.add("슬로바키아");
        countryList.add("슬로베니아");
        countryList.add("시리아");
        countryList.add("시에라리온");
        countryList.add("싱가포르");
        countryList.add("아랍에미리트");
        countryList.add("아르메니아");
        countryList.add("아르헨티나");
        countryList.add("아이슬란드");
        countryList.add("아이티");
        countryList.add("아일랜드");
        countryList.add("아제르바이잔");
        countryList.add("아프가니스탄");
        countryList.add("안도라");
        countryList.add("알바니아");
        countryList.add("알제리");
        countryList.add("압하스 자치공화국");
        countryList.add("앙골라");
        countryList.add("앤티가 바부다");
        countryList.add("에리트레아");
        countryList.add("에스토니아");
        countryList.add("에콰도르");
        countryList.add("에티오피아");
        countryList.add("엘살바도르");
        countryList.add("영국");
        countryList.add("예멘");
        countryList.add("오만");
        countryList.add("오스트레일리아");
        countryList.add("오스트리아");
        countryList.add("온두라스");
        countryList.add("요르단");
        countryList.add("우간다");
        countryList.add("우루과이");
        countryList.add("우즈베키스탄");
        countryList.add("우크라이나");
        countryList.add("이라크");
        countryList.add("이란");
        countryList.add("이스라엘");
        countryList.add("이집트");
        countryList.add("이탈리아");
        countryList.add("인도");
        countryList.add("인도네시아");
        countryList.add("일본");
        countryList.add("자메이카");
        countryList.add("잠비아");
        countryList.add("적도 기니");
        countryList.add("조선민주주의인민공화국");
        countryList.add("조지아");
        countryList.add("중국");
        countryList.add("중앙아프리카 공화국");
        countryList.add("지부티");
        countryList.add("짐바브웨");
        countryList.add("차드");
        countryList.add("체코");
        countryList.add("칠레");
        countryList.add("카메룬");
        countryList.add("카보베르데");
        countryList.add("카자흐스탄");
        countryList.add("카타르");
        countryList.add("캄보디아");
        countryList.add("캐나다");
        countryList.add("케냐");
        countryList.add("코모로");
        countryList.add("코소보공화국");
        countryList.add("코스타리카");
        countryList.add("코트디부아르");
        countryList.add("콜롬비아");
        countryList.add("콩고 공화국");
        countryList.add("콩고 민주 공화국");
        countryList.add("쿠바");
        countryList.add("쿠웨이트");
        countryList.add("크로아티아");
        countryList.add("키르기스스탄");
        countryList.add("키리바시");
        countryList.add("키프로스");
        countryList.add("타이완");
        countryList.add("타지키스탄");
        countryList.add("탄자니아");
        countryList.add("태국");
        countryList.add("터키");
        countryList.add("토고");
        countryList.add("통가");
        countryList.add("투르크메니스탄");
        countryList.add("투발루");
        countryList.add("튀니지");
        countryList.add("트리니다드 토바고");
        countryList.add("파나마");
        countryList.add("파라과이");
        countryList.add("파키스탄");
        countryList.add("파푸아뉴기니");
        countryList.add("팔라우");
        countryList.add("페루");
        countryList.add("팔레스타인");
        countryList.add("포르투갈");
        countryList.add("폴란드");
        countryList.add("프랑스");
        countryList.add("피지");
        countryList.add("핀란드");
        countryList.add("필리핀");
        countryList.add("헝가리");
    }
}
