package com.btjcenter.pray4m.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.adapter.PostListAdapter;
import com.btjcenter.pray4m.api.ListDeserializer;
import com.btjcenter.pray4m.api.PostService;
import com.btjcenter.pray4m.model.Post;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class SearchActivity extends BTJPrayerBaseActivity {

    EditText editText_search;
    ImageView imageView_ic_search;
    RecyclerView recyclerView_search;

    List<Post> posts;
    RecyclerView.Adapter adapter;
    LinearLayoutManager layoutManager;

    PostService postService;

    Boolean mLockListView;
    Boolean END_FLAG = false;
    Integer page;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        Type type = new TypeToken<Post>(){}.getType();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(type, new ListDeserializer<>(Post.class, "post")).create();;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.btjprayer.net")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        postService = retrofit.create(PostService.class);

        page = 1;

        posts = new ArrayList<Post>();

        editText_search = (EditText) findViewById(R.id.edittext_search);

        imageView_ic_search = (ImageView) findViewById(R.id.ic_search_search);
        recyclerView_search = (RecyclerView) findViewById(R.id.postlist_search);
        adapter = new PostListAdapter(posts, false);
        recyclerView_search.setAdapter(adapter);
        layoutManager = new LinearLayoutManager(this);
        recyclerView_search.setLayoutManager(layoutManager);
        recyclerView_search.setHasFixedSize(false);
        recyclerView_search.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = recyclerView.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                int count = totalItemCount - visibleItemCount;

                if(firstVisibleItem >= count && totalItemCount != 0 && !mLockListView && !END_FLAG) {
                    new GetSearchList(editText_search.getText().toString(), page).execute();
                    page += 1;
                }

            }
        });





        final Context context = this;

        imageView_ic_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText_search.clearFocus();
                page = 2;
                posts.clear();
                new GetSearchList(editText_search.getText().toString(), 1).execute();

                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);


            }
        });


        editText_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    imageView_ic_search.performClick();
                    return true;
                }

                return false;
            }
        });

    }


    public class GetSearchList extends AsyncTask<Void, Void, Void> {

        Type type;
        Gson gson;
        PostService postService;
        Retrofit retrofit;

        String keyword;
        int page;





        public GetSearchList(String _keyword, int _page){
            keyword = _keyword;
            page = _page;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            type = new TypeToken<List<Post>>() {
            }.getType();
            gson = new GsonBuilder()
                    .registerTypeAdapter(type, new ListDeserializer<>(List.class, "posts")).create();

            retrofit = new Retrofit.Builder()
                    .validateEagerly()
                    .baseUrl("http://www.btjprayer.net")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            postService = retrofit.create(PostService.class);

        }

        @Override
        protected Void doInBackground(Void... params) {
            mLockListView = true;

            Call<List<Post>> postscall = postService.getSearchPostList(keyword, page);
            postscall.enqueue(new Callback<List<Post>>() {
                @Override
                public void onResponse(Response<List<Post>> response, Retrofit retrofit) {
                    if(response != null) {
                        if(response.isSuccess()){
                            posts.addAll(response.body());
                            adapter.notifyDataSetChanged();
                            if(response.body().size()==0)
                                END_FLAG = true;
                            mLockListView = false;
                        }
                        //Log.d("SearchActivity", response.body().size() + "");
                    }
                }

                @Override
                public void onFailure(Throwable throwable) {

                }
            });



            return null;
        }

        @Override
        protected void onPostExecute(Void args){

        }
    }

}
