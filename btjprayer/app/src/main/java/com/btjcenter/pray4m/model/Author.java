package com.btjcenter.pray4m.model;

/**
 * Created by alexmoon on 2015. 11. 7..
 */
public class Author {
    Integer id;
    String slug;
    String name;
    String nickname;


    public Integer getId() {
        return id;
    }

    public String getSlug() {
        return slug;
    }

    public String getName() {
        return name;
    }

    public String getNickname() { return nickname; }
}
