package com.btjcenter.pray4m.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.btjcenter.pray4m.BiblePageholderFragment;
import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.adapter.BiblePagerAdapter;
import com.btjcenter.pray4m.db.MySQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class GapActivity extends BTJPrayerBaseActivity {
    static public String TAG = "GapActivity";
    BiblePagerAdapter biblePagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gap);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.97);
        getWindow().setLayout(screenWidth, WindowManager.LayoutParams.MATCH_PARENT);

        int count;

        MySQLiteOpenHelper DBhelper = new MySQLiteOpenHelper(this);
        SQLiteDatabase db = DBhelper.getReadableDatabase();

        int year = Calendar.getInstance().get(Calendar.YEAR);
        int day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);

        //Log.d(TAG, ""+year);

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        Cursor gapCursor;
        //윤년 처리
        if (gregorianCalendar.isLeapYear(year) && day >= 60) {
            gapCursor = db.rawQuery("SELECT * " +
                    "FROM gap " +
                    "WHERE day=" + (day - 1), null);
        } else {
            gapCursor = db.rawQuery("SELECT * " +
                    "FROM gap " +
                    "WHERE day=" + day, null);
        }

        gapCursor.moveToFirst();
        count = gapCursor.getInt(gapCursor.getColumnIndex("count"));

        String[] todaygap = new String[count];
        List<ArrayList<String>> verses = new ArrayList<ArrayList<String>>(count);
        int[] init = new int[count];
        int[] end = new int[count];
        int[] book = new int[count];
        int[] chapter = new int[count];

        for (int i = 0; i < count; i++) {
            todaygap[i] = gapCursor.getString(gapCursor.getColumnIndex("c1") + i);
            end[i] = 0;
            verses.add(new ArrayList<String>());
        }


        for (int j = 0; j < count; j++) {
            book[j] = Integer.parseInt(todaygap[j].split(";")[0]);
            chapter[j] = Integer.parseInt(todaygap[j].split(";")[1].split(":")[0]);
            if (todaygap[j].contains(":")) {
                init[j] = Integer.parseInt(todaygap[j].split(";")[1].split(":")[1].split("-")[0]);
                end[j] = Integer.parseInt(todaygap[j].split(";")[1].split(":")[1].split("-")[1]);
            } else
                init[j] = 1;


            if (todaygap[j].contains("-")) {
                Cursor bibleCursor = db.rawQuery("SELECT * " +
                        "FROM bible " +
                        "WHERE book=" + book[j] +
                        " and chapter=" + chapter[j] +
                        " and verse>=" + init[j] +
                        " and verse<=" + end[j] +
                        " order by verse asc", null);
                bibleCursor.moveToFirst();
                do {
                    verses.get(j).add(bibleCursor.getString(bibleCursor.getColumnIndex("content")));

                } while (bibleCursor.moveToNext());


                bibleCursor.close();
            } else if (todaygap[j].contains(":")) {
                Cursor bibleCursor = db.rawQuery("SELECT * " +
                        "FROM bible " +
                        "WHERE book=" + book[j] +
                        " and chapter=" + chapter[j] +
                        " and verse=" + init[j], null);
                bibleCursor.moveToFirst();
                do {
                    verses.get(j).add(bibleCursor.getString(bibleCursor.getColumnIndex("content")));

                } while (bibleCursor.moveToNext());


                bibleCursor.close();
            } else {
                Cursor bibleCursor = db.rawQuery("SELECT * " +
                        "FROM bible " +
                        "WHERE book=" + book[j] +
                        " and chapter=" + chapter[j] +
                        " order by verse asc", null);
                bibleCursor.moveToFirst();
                do {
                    verses.get(j).add(bibleCursor.getString(bibleCursor.getColumnIndex("content")));

                } while (bibleCursor.moveToNext());


                bibleCursor.close();
            }


        }


        gapCursor.close();
        DBhelper.close();

        final ViewPager pager_bible = (ViewPager) findViewById(R.id.pager_bible);
        biblePagerAdapter = new BiblePagerAdapter(this.getSupportFragmentManager(), book, chapter, init, verses);
        pager_bible.setAdapter(biblePagerAdapter);
        TabLayout tabs_bible = (TabLayout) findViewById(R.id.tabs_bible);
        tabs_bible.setupWithViewPager(pager_bible);
        tabs_bible.setTabMode(TabLayout.MODE_SCROLLABLE);
        changeTabsFont(tabs_bible);

        ImageView image_join = (ImageView) findViewById(R.id.button_join_gap);
        image_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), GapJoinActivity.class);
                startActivity(intent);
            }
        });
        ImageView image_info = (ImageView) findViewById(R.id.button_info_gap);
        image_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), GapInfoActivity.class);
                startActivity(intent);
            }
        });


        ImageView imageView = (ImageView) findViewById(R.id.image_copy);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w(TAG, "onClick Copy");
                if (biblePagerAdapter != null)
                    ((BiblePageholderFragment) biblePagerAdapter.getFragmentItem(pager_bible.getCurrentItem())).copyBible();


            }
        });


//        FloatingActionButton fab_inc = (FloatingActionButton) findViewById(R.id.fab_inc);
//        fab_inc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent textSize = new Intent("BIBLE_TEXTSIZE");
//                textSize.putExtra("bigger", true);
//                sendBroadcast(textSize);
//            }
//        });
//        FloatingActionButton fab_dec = (FloatingActionButton) findViewById(R.id.fab_dec);
//        fab_dec.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent textSize = new Intent("BIBLE_TEXTSIZE");
//                textSize.putExtra("bigger", false);
//                sendBroadcast(textSize);
//            }
//        });
    }

    private void changeTabsFont(TabLayout tabLayout) {
        Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/NanumBarunGothic.otf");
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(font, Typeface.NORMAL);
                }
            }
        }
    }

}
