package com.btjcenter.pray4m.thread;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.btjcenter.pray4m.NationNumberProvider;
import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.api.ListDeserializer;
import com.btjcenter.pray4m.api.PostService;
import com.btjcenter.pray4m.model.Post;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by alexmoon on 2016. 3. 5..
 */
public class GetNewNations extends AsyncTask<Void, Void, Void>{
    Type type;
    Gson gson;
    PostService postService;
    Retrofit retrofit;

    List<Post> posts;

    View rootView;
    Context context;

    int[] tag_new = {
            R.id.image_tagnew_nations1,
            R.id.image_tagnew_nations1,
            R.id.image_tagnew_nations2,
            R.id.image_tagnew_nations3,
            R.id.image_tagnew_nations4,
            R.id.image_tagnew_nations5,
            R.id.image_tagnew_nations6,
            R.id.image_tagnew_nations7,
            R.id.image_tagnew_nations8,
            R.id.image_tagnew_nations9,
            R.id.image_tagnew_nations10,
            R.id.image_tagnew_nations11,
            R.id.image_tagnew_nations12,
            R.id.image_tagnew_nations13,
            R.id.image_tagnew_nations14,
            R.id.image_tagnew_nations15,
            R.id.image_tagnew_nations16,
            R.id.image_tagnew_nations17,
            R.id.image_tagnew_nations18,
            R.id.image_tagnew_nations19,
            R.id.image_tagnew_nations20,
            R.id.image_tagnew_nations21,
            R.id.image_tagnew_nations22,
            R.id.image_tagnew_nations23,
            R.id.image_tagnew_nations24,
            R.id.image_tagnew_nations25,
            R.id.image_tagnew_nations26,
            R.id.image_tagnew_nations27,
            R.id.image_tagnew_nations28,
            R.id.image_tagnew_nations29,
            R.id.image_tagnew_nations30,
            R.id.image_tagnew_nations31,
            R.id.image_tagnew_nations32,
            R.id.image_tagnew_nations33,
            R.id.image_tagnew_nations34,
            R.id.image_tagnew_nations35,
            R.id.image_tagnew_nations36,
            R.id.image_tagnew_nations37,
            R.id.image_tagnew_nations38,
            R.id.image_tagnew_nations_georgia,
    };

    public GetNewNations(View rv){
        rootView = rv;
    }

    @Override
    protected void onPreExecute() {
        posts = new ArrayList<Post>();

        type = new TypeToken<List<Post>>(){}.getType();
        gson = new GsonBuilder()
                .registerTypeAdapter(type, new ListDeserializer<>(List.class, "posts")).create();

        retrofit = new Retrofit.Builder()
                .validateEagerly()
                .baseUrl("http://www.btjprayer.net")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        postService = retrofit.create(PostService.class);
    }

    @Override
    protected Void doInBackground(Void... params) {
        Call<List<Post>> nationsCall = postService.getNewNations();
        nationsCall.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Response<List<Post>> response, Retrofit retrofit) {
                if(response != null && response.isSuccess()){
                    posts.addAll(response.body());
                    for(int i = 0; i < posts.size(); i ++){
                        Post post = posts.get(i);
                        int year_modified = Integer.parseInt(post.getModified().substring(0, 4));
                        int month_modified = Integer.parseInt(post.getModified().substring(5, 7));
                        int day_modified = Integer.parseInt(post.getModified().substring(8, 10));
                        int hour_modified = Integer.parseInt(post.getModified().substring(11, 13));;
                        int min_modified = Integer.parseInt(post.getModified().substring(14, 16));;
                        int sec_modified = Integer.parseInt(post.getModified().substring(17));
                        Calendar modified = Calendar.getInstance();
                        modified.set(year_modified, month_modified-1, day_modified, hour_modified, min_modified, sec_modified);

                        Calendar current = Calendar.getInstance();

                        if(new NationNumberProvider(post.getId()).getNationNumber() != 0) {
                            ImageView tag = (ImageView) rootView.findViewById(tag_new[new NationNumberProvider(post.getId()).getNationNumber()]);
                            if (current.getTimeInMillis() - modified.getTimeInMillis() < 3 * 24 * 60 * 60 * 1000) {
                                tag.setVisibility(View.VISIBLE);
                            }
                        }

                        //Log.d("GetNewNations", (current.getTimeInMillis() - modified.getTimeInMillis()) + " milliseconds");


                    }


                }

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        Log.d("GetNewNations", posts.size() + "");

    }
}
