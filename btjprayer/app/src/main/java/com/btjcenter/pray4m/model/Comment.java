package com.btjcenter.pray4m.model;

/**
 * Created by alexmoon on 2015. 11. 7..
 */
public class Comment {
    Integer id;
    String name;
    String date;
    String content;
    Integer parent;
    String category;
    String parent_title;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getContent() {
        return content.replaceAll("(&)(#)8216;", "'")
                .replaceAll("(&)(#)8217;", "'")
                .replaceAll("(&)(#)8220;", "\"")
                .replaceAll("(&)(#)8221;", "\"")
                .replaceAll("(&)(#)8230;", "...");
    }

    public Integer getParent() {
        return parent;
    }

    public String getCategory() {
        return category;
    }

    public String getParent_title() {
        return parent_title;
    }
}
