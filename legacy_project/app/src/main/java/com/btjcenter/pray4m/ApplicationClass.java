package com.btjcenter.pray4m;

import android.app.Application;

import com.tsengvn.typekit.Typekit;

/**
 * Created by alexmoon on 2016. 3. 16..
 */
public class ApplicationClass extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "fonts/NanumBarunGothic.otf"))
                .addBold(Typekit.createFromAsset(this, "fonts/NanumBarunGothicBold.otf"));
    }
}
