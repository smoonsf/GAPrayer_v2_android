package com.btjcenter.pray4m.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.activity.PostActivity;
import com.btjcenter.pray4m.model.Post;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by alexmoon on 2015. 11. 9..
 */
public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.ViewHolder> {
    List<Post> posts;
    View rootView;
    boolean date_tag;

    class ViewHolder extends RecyclerView.ViewHolder{
        public TextView m_title;
        public TextView m_date_author;
        public TextView m_date_tag;
        public ImageView m_image;
        public ViewHolder(View v) {
            super(v);
            this.m_title = (TextView) v.findViewById(R.id.title_postlist_item);
            this.m_date_author = (TextView) v.findViewById(R.id.date_postlist_item);
            this.m_date_tag = (TextView) v.findViewById(R.id.datetag_postlist_item);
            this.m_image = (ImageView) v.findViewById(R.id.image_postlist_item);
        }
    }

    public PostListAdapter(List<Post> postlist, boolean _date_tag){
        posts = postlist;
        date_tag = _date_tag;
    }

    @Override
    public PostListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder =
                new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_postlist, null));


        return holder;
    }

    @Override
    public void onBindViewHolder(PostListAdapter.ViewHolder holder, int position) {
        final Post post = posts.get(position);

        int year_prev = 0;
        int month_prev = 0;
        int day_prev = 0;

        if(position > 0){
            year_prev = Integer.parseInt(posts.get(position-1).getDate().substring(0, 4));
            month_prev = Integer.parseInt(posts.get(position-1).getDate().substring(5, 7));
            day_prev = Integer.parseInt(posts.get(position-1).getDate().substring(8, 10));
        }
        int year = Integer.parseInt(post.getDate().substring(0, 4));
        int month = Integer.parseInt(post.getDate().substring(5, 7));
        int day = Integer.parseInt(post.getDate().substring(8, 10));
        Calendar calendar = new GregorianCalendar(year, month - 1, day);
        String dayString = "";
        switch (calendar.get(Calendar.DAY_OF_WEEK)){
            case 1: dayString = "(일)"; break;
            case 2: dayString = "(월)"; break;
            case 3: dayString = "(화)"; break;
            case 4: dayString = "(수)"; break;
            case 5: dayString = "(목)"; break;
            case 6: dayString = "(금)"; break;
            case 7: dayString = "(토)"; break;
        }
        String dateString = month + "월 " + day + "일 " + dayString;




        holder.m_title.setText(post.getTitle_plain());
        holder.m_date_author.setText(year + "년 " + dateString);
        holder.m_date_tag.setText(dateString);

        if(date_tag){
            if(year == year_prev && month == month_prev && day == day_prev){
                holder.m_date_tag.setVisibility(View.GONE);
            } else {
                holder.m_date_tag.setVisibility(View.VISIBLE);
            }
            holder.m_date_author.setVisibility(View.GONE);
            holder.m_title.setMaxLines(3);
        } else {
            holder.m_date_tag.setVisibility(View.GONE);
        }


        if(post.getAttachments().size() != 0)
            Picasso.with(holder.itemView.getContext())
                    .load(post.getAttachments().get(0).getUrl())
                    .placeholder(R.drawable.temp)
                    .into(holder.m_image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), PostActivity.class);
                intent.putExtra("id", post.getId());
                if(post.getCategories().get(0).getSlug().contains("nationsjoin"))
                    intent.putExtra("nation", true);

                Log.d("PostListAdapter", post.getCategories().get(0).getSlug());

                v.getContext().startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return posts.size();
    }


}
