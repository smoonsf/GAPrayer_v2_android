package com.btjcenter.pray4m.model;

/**
 * Created by alexmoon on 2015. 11. 7..
 */
public class Category {
    Integer id;
    String slug;
    String title;
    Integer post_count;

    public Integer getId() {
        return id;
    }

    public String getSlug() {
        return slug;
    }

    public String getTitle() {
        return title;
    }

    public Integer getPost_count() {
        return post_count;
    }
}
