package com.btjcenter.pray4m.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.btjcenter.pray4m.HomePageholderFragment;

/**
 * Created by alexmoon on 2015. 11. 13..
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {


    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        return HomePageholderFragment.create(position);
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        switch (position){
            case 0:
                title = "HOME";
                break;
            case 1:
                title = "오늘의 기도";
                break;
            case 2:
                title = "종족별 기도";
                break;
//            case 3:
//                title = "많이 본 글";
//                break;
            case 3:
                title = "BTJ 자료실";
                break;
            case 4:
                title = "이전 글";
                break;
        }
        return title;
    }
}
