package com.btjcenter.pray4m.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.text.Html;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.activity.SplashActivity;
import com.google.android.gms.gcm.GcmListenerService;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by saltfactory on 6/8/15.
 */
public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    Bitmap bigIcon;


    /**
     *
     * @param from SenderID 값을 받아온다.
     * @param extras Set형태로 GCM으로 받은 데이터 payload이다.
     */
    @Override
    public void onMessageReceived(String from, Bundle extras) {
        bigIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification_large);


        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        if (!extras.isEmpty()) {
            if(extras.getString("message") != null){
                sendMessageNotification(extras.getString("message"));
            }else if(extras.getString("update") != null) {
                sendUpdateNotification(extras.getString("update"));
            }else if(extras.getString("new_post") != null) {
                sendNewNotification(extras.getString("new_post"));
            }
        }


        // GCM으로 받은 메세지를 디바이스에 알려주는 sendNotification()을 호출한다.
        //sendNotification("오늘의 기도", "");
    }


    /**
     * 실제 디바에스에 GCM으로부터 받은 메세지를 알려주는 함수이다. 디바이스 Notification Center에 나타난다.
     * @param title
     * @param message
     */
    private void sendNotification(String title, String message) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(bigIcon)
                .setSmallIcon(R.drawable.ic_notification_small)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void sendMessageNotification(String msg) {
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("msg",msg);
        intent.putExtra("todo","message");
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,intent, PendingIntent.FLAG_CANCEL_CURRENT);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean enable_message = prefs.getBoolean("pref_key_push_msg", true);
        boolean notificate = prefs.getBoolean("notifications", true);
        boolean vibrate = prefs.getBoolean("notifications_vibrate", true);
        Uri uri = Uri.parse(prefs.getString("notifications_ringtone", "none"));


        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this)
                .setLargeIcon(bigIcon)
                .setSmallIcon(R.drawable.ic_notification_small)
                .setSound(uri)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg)
                .setContentTitle(getString(R.string.app_name))
                .setContentIntent(contentIntent);

        if (enable_message && vibrate && notificate) {
            mBuilder.setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE);
            mNotificationManager.notify(0, mBuilder.build());
        }else if(enable_message && !vibrate && notificate){
            mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
            mNotificationManager.notify(0, mBuilder.build());
        }
    }

    private void sendUpdateNotification(String post) {
        String p = post;
        String[] ps = p.split(";");
        String title = ps[0];
        String url = ps[1];
        String id = ps[2];
        String author = ps[3];

        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("todo","updatePost");
        intent.putExtra("post_title", title);
        intent.putExtra("post_url", url);
        intent.putExtra("post_id", id);
        intent.putExtra("post_author", author);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,intent, PendingIntent.FLAG_CANCEL_CURRENT);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean enable_update = prefs.getBoolean("pref_key_push_new", true);
        boolean notificate = prefs.getBoolean("notifications", true);
        boolean vibrate = prefs.getBoolean("notifications_vibrate", true);
        Uri uri = Uri.parse(prefs.getString("notifications_ringtone", "none"));

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this)
                .setLargeIcon(bigIcon)
                .setSmallIcon(R.drawable.ic_notification_small)
                .setSound(uri)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml("<b>" + title + "</b> was updated")))
                .setContentText(title+Html.fromHtml("<b>"+title+"</b> was updated"))
                .setContentTitle(getString(R.string.app_name))
                .setContentIntent(contentIntent);

        if (enable_update && vibrate && notificate) {
            mBuilder.setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE);
            mNotificationManager.notify(0, mBuilder.build());
        }else if(enable_update && !vibrate && notificate){
            mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
            mNotificationManager.notify(0, mBuilder.build());
        }
    }

    private void sendNewNotification(String post) {
        String p = post;
        String[] ps = p.split(";");
        String title = ps[0];
        String url = ps[1];
        String id = ps[2];
        String author = ps[3];

        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("todo","updatePost");
        intent.putExtra("post_title", title);
        intent.putExtra("post_url", url);
        intent.putExtra("post_id", id);
        intent.putExtra("post_author", author);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,intent, PendingIntent.FLAG_CANCEL_CURRENT);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean enable_new = prefs.getBoolean("pref_key_push_new", true);
        boolean notificate = prefs.getBoolean("notifications", true);
        boolean vibrate = prefs.getBoolean("notifications_vibrate", true);
        Uri uri = Uri.parse(prefs.getString("notifications_ringtone", "none"));

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this)
                .setLargeIcon(bigIcon)
                .setSmallIcon(R.drawable.ic_notification_small)
                .setSound(uri)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(Html.fromHtml("New Post released <b>"+title+"</b>")))
                .setContentText(Html.fromHtml("New Post released <b>"+title+"</b>"))
                .setContentTitle(getString(R.string.app_name))
                .setContentIntent(contentIntent);

        if (enable_new && vibrate && notificate) {
            mBuilder.setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE);
            mNotificationManager.notify(0, mBuilder.build());
        }else if(enable_new && !vibrate && notificate){
            mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
            mNotificationManager.notify(0, mBuilder.build());
        }
    }
}
