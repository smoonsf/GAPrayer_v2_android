package com.btjcenter.pray4m.model;

/**
 * Created by alexmoon on 2015. 11. 7..
 */
public class Attachment {
    Integer id;
    String url;
    Integer parent;
    String mime_type;

    public Integer getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public Integer getParent() {
        return parent;
    }

    public String getMime_type() {
        return mime_type;
    }
}
