package com.btjcenter.pray4m.adapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.btjcenter.pray4m.AlarmReceiver;
import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.activity.SetAlarmActivity;
import com.btjcenter.pray4m.db.TinyDB;

import java.util.ArrayList;

/**
 * Created by alexmoon on 2016. 2. 4..
 */
public class AlarmListAdapter extends RecyclerView.Adapter<AlarmListAdapter.ViewHolder> {
    ArrayList<String> alarms;
    Context context;


    public AlarmListAdapter(Context ctx){
        context = ctx;
        TinyDB tinyDB = new TinyDB(context.getApplicationContext());
        alarms = tinyDB.getListString("alarms");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder =
                new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alarmlist, null));
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String[] alarmInfo = alarms.get(position).split(";");

        boolean enabled = Boolean.parseBoolean(alarmInfo[0]);

        String ampm = "am";
        int hour = Integer.parseInt(alarmInfo[1]);
        if(hour >= 12)
            ampm = "pm";
        if(hour > 12)
            hour -= 12;
        int min = Integer.parseInt(alarmInfo[2]);

        boolean mon = Boolean.parseBoolean(alarmInfo[3]);
        boolean tue = Boolean.parseBoolean(alarmInfo[4]);
        boolean wed = Boolean.parseBoolean(alarmInfo[5]);
        boolean thu = Boolean.parseBoolean(alarmInfo[6]);
        boolean fri = Boolean.parseBoolean(alarmInfo[7]);
        boolean sat = Boolean.parseBoolean(alarmInfo[8]);
        boolean sun = Boolean.parseBoolean(alarmInfo[9]);


        String memo = "";
        if(alarmInfo.length == 11)
            memo = alarmInfo[10];
        String time;
        time = "" + hour + ":" + min + " " + ampm;
        if(min < 10)
            time = "" + hour + ":0" + min + " " + ampm;
        String dayrepeat = "";
        if(mon)
            dayrepeat += "월";
        if(tue)
            dayrepeat += "화";
        if(wed)
            dayrepeat += "수";
        if(thu)
            dayrepeat += "목";
        if(fri)
            dayrepeat += "금";
        if(sat)
            dayrepeat += "토";
        if(sun)
            dayrepeat += "일";

        if(!mon&&!tue&&!wed&&!thu&&!fri&&!sat&&!sun)
            dayrepeat = "반복 사용 안함";

        holder.m_enabled.setChecked(enabled);
        holder.m_enabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                TinyDB tinyDB = new TinyDB(context.getApplicationContext());
                String newAlarmInfo = isChecked + ";" + alarmInfo[1]
                        + ";" + alarmInfo[2]
                        + ";" + alarmInfo[3]
                        + ";" + alarmInfo[4]
                        + ";" + alarmInfo[5]
                        + ";" + alarmInfo[6]
                        + ";" + alarmInfo[7]
                        + ";" + alarmInfo[8]
                        + ";" + alarmInfo[9]
                        + ";";
                if(alarmInfo.length == 11)
                    newAlarmInfo += alarmInfo[10];
                alarms.set(position, newAlarmInfo);
                tinyDB.putListString("alarms", alarms);
                AlarmReceiver alarmReceiver = new AlarmReceiver();
                alarmReceiver.cancelAlarm(context.getApplicationContext());
                alarmReceiver.setAlarm(context.getApplicationContext());
            }
        });
        holder.m_memo.setText(memo);
        holder.m_time.setText(time);
        holder.m_dayrepeat.setText(dayrepeat);

        holder.m_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                TinyDB tinyDB = new TinyDB(context.getApplicationContext());
                ArrayList<Boolean> delete_list = tinyDB.getListBoolean("delete_list");
                if (isChecked) {
                    delete_list.set(position, true);
                } else {
                    delete_list.set(position, false);
                }
                tinyDB.putListBoolean("delete_list", delete_list);
            }
        });


        holder.m_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SetAlarmActivity.class);
                intent.putExtra("index", position);
//                if(!holder.m_check.isChecked())
//                    holder.m_check.performClick();

                context.startActivity(intent);
            }
        });

        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getBooleanExtra("delete_mode", false))
                    holder.m_check.setVisibility(View.VISIBLE);
                else {
                    holder.m_check.setVisibility(View.GONE);
                }

            }
        };
        IntentFilter intFilt = new IntentFilter("ENABLE_CHECK");
        context.registerReceiver(broadcastReceiver, intFilt);

    }

    @Override
    public int getItemCount() {
        return alarms.size();
    }

    public void update(){
        alarms.clear();
        TinyDB tinyDB = new TinyDB(context.getApplicationContext());
        alarms.addAll(tinyDB.getListString("alarms"));
        super.notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        public Switch m_enabled;
        public TextView m_memo;
        public TextView m_time;
        public CheckBox m_check;
        public TextView m_dayrepeat;
        public ImageView m_edit;
        public ViewHolder(View v) {
            super(v);
            this.m_enabled = (Switch) v.findViewById(R.id.switch_enable_alarmitem);
            this.m_memo = (TextView) v.findViewById(R.id.memo_alarmitem);
            this.m_time = (TextView) v.findViewById(R.id.time_alarmitem);
            this.m_check = (CheckBox) v.findViewById(R.id.check_alarmitem);
            this.m_dayrepeat = (TextView) v.findViewById(R.id.dayrepeat_alarmtime);
            this.m_edit = (ImageView) v.findViewById(R.id.image_edit_alarmitem);
        }
    }
}
