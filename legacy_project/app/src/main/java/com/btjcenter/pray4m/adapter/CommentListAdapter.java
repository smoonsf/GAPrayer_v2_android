package com.btjcenter.pray4m.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.model.Comment;

import java.util.List;

/**
 * Created by alexmoon on 2015. 11. 9..
 */
public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.ViewHolder> {
    List<Comment> comments;

    class ViewHolder extends RecyclerView.ViewHolder{
        public TextView m_name;
        public TextView m_date;
        public TextView m_content;
        public ViewHolder(View v) {
            super(v);
            this.m_name = (TextView) v.findViewById(R.id.name_comment);
            this.m_date = (TextView) v.findViewById(R.id.date_comment);
            this.m_content = (TextView) v.findViewById(R.id.content_comment);
        }
    }

    public CommentListAdapter(List<Comment> commentslist){
        comments = commentslist;
    }

    @Override
    public CommentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder =
                new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_commentlist, null));


        return holder;
    }

    @Override
    public void onBindViewHolder(CommentListAdapter.ViewHolder holder, int position) {
        int reverse_position = comments.size() - position - 1;
        final Comment comment = comments.get(reverse_position);
        holder.m_name.setText(comment.getName());
        holder.m_date.setText(comment.getDate());
        holder.m_content.setText(comment.getContent());
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }


}
