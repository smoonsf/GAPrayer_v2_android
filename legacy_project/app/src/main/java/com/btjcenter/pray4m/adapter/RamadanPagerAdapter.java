package com.btjcenter.pray4m.adapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ListAdapter;

import com.btjcenter.pray4m.BiblePageholderFragment;
import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.activity.CommentActivity;
import com.btjcenter.pray4m.api.ListDeserializer;
import com.btjcenter.pray4m.api.PostService;
import com.btjcenter.pray4m.model.Comment;
import com.btjcenter.pray4m.model.Post;
import com.btjcenter.pray4m.utils.DateProvider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by alexmoon on 2015. 11. 13..
 */
public class RamadanPagerAdapter extends FragmentStatePagerAdapter {

    FragmentManager fragmentManager;
    int mode;


    public RamadanPagerAdapter(FragmentManager fm, int _mode) {
        super(fm);
        fragmentManager = fm;
        mode = _mode;
    }

    @Override
    public Fragment getItem(int position) {

        return RamadanPageholderFragment.create(position, mode);
    }

    @Override
    public int getCount() {
        Calendar currentDate = Calendar.getInstance();
        int current_doy = currentDate.get(Calendar.DAY_OF_YEAR);
        int start_day = new DateProvider().RAMADAN16_START.get(Calendar.DAY_OF_YEAR);
        Log.d("RamadanPagerAdapter", (current_doy - start_day) + "");

        if(current_doy - start_day < 0){
            return 1;
        } else if (current_doy - start_day >= 29) {
            return 31;
        } else {
            return current_doy - start_day + 2;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0)
            return "라마단이란";
        else {
            return "Day " + position;
        }
    }

    public static class RamadanPageholderFragment extends Fragment {
        final private int LANG_KOR = 0;
        final private int LANG_ENG = 1;
        final private int LANG_CN = 2;
        final private int LANG_JAP = 3;

        private int day;
        private int language_mode;
        private int post_id;

        private RecyclerView commentList;
        private LinearLayoutManager layoutManager;
        private CommentListAdapter adapter;
        private List<Comment> comments;
        private Button submit;
        private WebView webView;

        static public RamadanPageholderFragment create(int day, int mode){
            RamadanPageholderFragment fragment = new RamadanPageholderFragment();
            Bundle args = new Bundle();
            args.putInt("day", day);
            args.putInt("mode", mode);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            day = getArguments().getInt("day");
            language_mode = getArguments().getInt("mode");
            post_id = getRamadanPostId(day, language_mode);
            comments = new ArrayList<Comment>();


        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_ramadan, container, false);
            webView = (WebView) rootView.findViewById(R.id.webview_ramadan);
            submit = (Button) rootView.findViewById(R.id.btn_submit);
            commentList = (RecyclerView) rootView.findViewById(R.id.recycler_ramadan);
            switch (language_mode){
                case LANG_ENG:
                    submit.setText("Share Prayer Request");
                    break;
                case LANG_CN:
                    submit.setText("写代祷文");
                    break;
                case LANG_JAP:
                    submit.setText("祈りの文の投稿");
                    break;
            }

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setAppCacheEnabled(true);
            webView.getSettings().setDomStorageEnabled(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                    view.reload();
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    commentList.setVisibility(View.VISIBLE);
                    commentList.stopScroll();
                    submit.setVisibility(View.VISIBLE);
                }
            });

            String url = "http://www.btjprayer.net/archives/" + post_id + "?mobile=true";
            webView.loadUrl(url);

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), CommentActivity.class);
                    intent.putExtra("id", post_id);
                    intent.putExtra("LANG_MODE", language_mode);
                    startActivity(intent);
                }
            });


            layoutManager = new LinearLayoutManager(rootView.getContext());
            commentList.setLayoutManager(layoutManager);
            adapter = new CommentListAdapter(comments);
            commentList.setAdapter(adapter);

            BroadcastReceiver toDayBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(day==0)
                        webView.onPause();
                }
            };
            IntentFilter intDayFilt = new IntentFilter("TO_DAY_TAB");
            getActivity().registerReceiver(toDayBroadcastReceiver, intDayFilt);

            BroadcastReceiver toVideoBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(day==0)
                        webView.onResume();
                }
            };
            IntentFilter intVideoFilt = new IntentFilter("TO_VIDEO_TAB");
            getActivity().registerReceiver(toVideoBroadcastReceiver, intVideoFilt);

            return rootView;
        }

        @Override
        public void onStart() {
            super.onStart();
            comments.clear();
            Type type = new TypeToken<Post>(){}.getType();
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(type, new ListDeserializer<>(Post.class, "post")).create();;
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://www.btjprayer.net")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            PostService postService = retrofit.create(PostService.class);

            final Call<Post> postcall = postService.getPost(post_id);
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    postcall.enqueue(new Callback<Post>() {
                        @Override
                        public void onResponse(final Response<Post> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                comments.addAll(response.body().getComments());
                                //Log.d(TAG, comments.size() + "");
                                adapter.notifyDataSetChanged();
                                //layoutManager.notify();
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                        }
                    });
                }
            };

            Handler handler = new Handler();
            handler.postDelayed(runnable, 1000);
        }

        @Override
        public void onResume() {
            super.onResume();

        }

        private int getRamadanPostId(int _day, int _mode){
            switch (_mode){
                case LANG_KOR:
                    switch (_day){
                        case 1:return 11823;
                        case 2:return 11827;
                        case 3:return 11837;
                        case 4:return 11839;
                        case 5:return 11841;
                        case 6:return 11843;
                        case 7:return 11845;
                        case 8:return 11847;
                        case 9:return 11849;
                        case 10:return 11851;
                        case 11:return 11853;
                        case 12:return 11856;
                        case 13:return 11858;
                        case 14:return 11861;
                        case 15:return 11863;
                        case 16:return 11865;
                        case 17:return 11867;
                        case 18:return 11869;
                        case 19:return 11871;
                        case 20:return 11873;
                        case 21:return 11875;
                        case 22:return 11877;
                        case 23:return 11879;
                        case 24:return 11881;
                        case 25:return 11883;
                        case 26:return 11885;
                        case 27:return 11887;
                        case 28:return 11889;
                        case 29:return 11891;
                        case 30:return 11893;
                    }
                case LANG_ENG:
                    switch (_day){
                        case 1:return 11895;
                        case 2:return 11897;
                        case 3:return 11899;
                        case 4:return 11901;
                        case 5:return 11903;
                        case 6:return 11905;
                        case 7:return 11907;
                        case 8:return 11909;
                        case 9:return 11911;
                        case 10:return 11913;
                        case 11:return 11915;
                        case 12:return 11917;
                        case 13:return 11919;
                        case 14:return 11921;
                        case 15:return 11923;
                        case 16:return 11925;
                        case 17:return 11927;
                        case 18:return 11929;
                        case 19:return 11931;
                        case 20:return 11933;
                        case 21:return 11935;
                        case 22:return 11937;
                        case 23:return 11939;
                        case 24:return 11941;
                        case 25:return 11943;
                        case 26:return 11945;
                        case 27:return 11947;
                        case 28:return 11949;
                        case 29:return 11951;
                        case 30:return 11953;
                    }
                case LANG_CN:
                    switch (_day){
                        case 1:return 11955;
                        case 2:return 11957;
                        case 3:return 11959;
                        case 4:return 11961;
                        case 5:return 11963;
                        case 6:return 11965;
                        case 7:return 11967;
                        case 8:return 11969;
                        case 9:return 11971;
                        case 10:return 11973;
                        case 11:return 11975;
                        case 12:return 11977;
                        case 13:return 11979;
                        case 14:return 11981;
                        case 15:return 11983;
                        case 16:return 11985;
                        case 17:return 11987;
                        case 18:return 11989;
                        case 19:return 11991;
                        case 20:return 11993;
                        case 21:return 11995;
                        case 22:return 11997;
                        case 23:return 11999;
                        case 24:return 12001;
                        case 25:return 12003;
                        case 26:return 12005;
                        case 27:return 12007;
                        case 28:return 12009;
                        case 29:return 12011;
                        case 30:return 12013;
                    }
                case LANG_JAP:
                    switch (_day){
                        case 1:return 12370;
                        case 2:return 12373;
                        case 3:return 12376;
                        case 4:return 12378;
                        case 5:return 12380;
                        case 6:return 12382;
                        case 7:return 12384;
                        case 8:return 12386;
                        case 9:return 12388;
                        case 10:return 12390;
                        case 11:return 12392;
                        case 12:return 12394;
                        case 13:return 12396;
                        case 14:return 12398;
                        case 15:return 12400;
                        case 16:return 12402;
                        case 17:return 12404;
                        case 18:return 12406;
                        case 19:return 12408;
                        case 20:return 12410;
                        case 21:return 12412;
                        case 22:return 12414;
                        case 23:return 12416;
                        case 24:return 12418;
                        case 25:return 12420;
                        case 26:return 12422;
                        case 27:return 12424;
                        case 28:return 12426;
                        case 29:return 12428;
                        case 30:return 12430;
                    }
                default:
                    return 12511;
            }
        }
    }

}
