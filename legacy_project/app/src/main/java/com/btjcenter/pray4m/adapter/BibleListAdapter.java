package com.btjcenter.pray4m.adapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.btjcenter.pray4m.R;

import java.util.ArrayList;

/**
 * Created by alexmoon on 2015. 11. 9..
 */
public class BibleListAdapter extends RecyclerView.Adapter<BibleListAdapter.ViewHolder> {
    ArrayList<String> content;
    int init;
    View rootView;

    class ViewHolder extends RecyclerView.ViewHolder{
        public TextView m_verse;
        public TextView m_content;
        public ViewHolder(View v) {
            super(v);
            this.m_verse = (TextView) v.findViewById(R.id.verse_bible);
            this.m_content = (TextView) v.findViewById(R.id.content_bible);
        }
    }

    public BibleListAdapter(int _init, ArrayList<String> _content){
        content = _content;
        init = _init;
    }

    @Override
    public BibleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder =
                new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bible, null));


        return holder;
    }

    @Override
    public void onBindViewHolder(final BibleListAdapter.ViewHolder holder, final int position) {
        holder.m_verse.setText(""+(init+position));
        holder.m_content.setText(content.get(position));

//        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                float textSize = holder.m_content.getTextSize();
//                if(intent.getBooleanExtra("bigger", false)){
//                    holder.m_content.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize + 6);
//                } else {
//                    holder.m_content.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize - 6);
//                }
//                notifyItemChanged(position);
//            }
//        };
//        IntentFilter intFilt = new IntentFilter("BIBLE_TEXTSIZE");
//        holder.itemView.getContext().registerReceiver(broadcastReceiver, intFilt);

    }

    @Override
    public int getItemCount() {
        return content.size();
    }


}
