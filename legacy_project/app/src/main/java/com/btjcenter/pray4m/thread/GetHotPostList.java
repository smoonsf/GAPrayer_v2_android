package com.btjcenter.pray4m.thread;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.adapter.GridListAdapter;
import com.btjcenter.pray4m.api.ListDeserializer;
import com.btjcenter.pray4m.api.PostService;
import com.btjcenter.pray4m.model.Post;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;


/**
 * Created by alexmoon on 2015. 11. 9..
 */
public class GetHotPostList extends AsyncTask<Void, Void, Void> {

    Type type;
    Gson gson;
    PostService postService;
    Retrofit retrofit;

    List<Post> posts;

    RecyclerView.Adapter adapter;
    RecyclerView postList;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridManager;
    TextView test;

    View rootView;
    Context context;




    public GetHotPostList(View rv){
        rootView = rv;
        context = rv.getContext();

        postList = (RecyclerView) rootView.findViewById(R.id.postlist_best);
        layoutManager = new LinearLayoutManager(context);
        gridManager = new GridLayoutManager(context, 2);
        postList.setHasFixedSize(false);
        postList.setLayoutManager(gridManager);
        test = (TextView) rootView.findViewById(R.id.test);
        posts = new ArrayList<Post>();

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        adapter = new GridListAdapter(posts, rootView);
        postList.setAdapter(adapter);

        type = new TypeToken<List<Post>>(){}.getType();
        gson = new GsonBuilder()
                .registerTypeAdapter(type, new ListDeserializer<>(List.class, "posts")).create();

        retrofit = new Retrofit.Builder()
                .validateEagerly()
                .baseUrl("http://www.btjprayer.net")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        postService = retrofit.create(PostService.class);
    }

    @Override
    protected Void doInBackground(Void... params) {
        Call<List<Post>> postscall = postService.getHotPost();
        postscall.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Response<List<Post>> response, Retrofit retrofit) {
                if(response.isSuccess()){
                    posts.addAll(response.body());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                new GetHotPostList(rootView).execute();
            }
        });



        return null;
    }

    @Override
    protected void onPostExecute(Void args){

    }
}
