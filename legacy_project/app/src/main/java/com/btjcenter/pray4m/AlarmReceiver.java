package com.btjcenter.pray4m;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.btjcenter.pray4m.db.TinyDB;

import java.util.ArrayList;
import java.util.Calendar;

public class AlarmReceiver extends WakefulBroadcastReceiver {

    // The app's AlarmManager, which provides access to the system alarm services.
    private AlarmManager alarmMgr;
    // The pending intent that is triggered when the alarm fires.
    private PendingIntent[] alarmIntent;




    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, WakeService.class);
        service.putExtra("index", intent.getIntExtra("index", -1));
        startWakefulService(context, service);
    }


    public void setAlarm(Context context){
        alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        TinyDB tinyDB;
        ArrayList<String> alarms;
        tinyDB = new TinyDB(context.getApplicationContext());
        alarms = tinyDB.getListString("alarms");
        alarmIntent = new PendingIntent[alarms.size()];

        for(int i = 0;i < alarms.size();i++){
            String[] alarminfo = alarms.get(i).split(";");
            boolean enabled = Boolean.parseBoolean(alarminfo[0]);
            int hour = Integer.parseInt(alarminfo[1]);
            int min = Integer.parseInt(alarminfo[2]);
            boolean mon = Boolean.parseBoolean(alarminfo[3]);
            boolean tue = Boolean.parseBoolean(alarminfo[4]);
            boolean wed = Boolean.parseBoolean(alarminfo[5]);
            boolean thu = Boolean.parseBoolean(alarminfo[6]);
            boolean fri = Boolean.parseBoolean(alarminfo[7]);
            boolean sat = Boolean.parseBoolean(alarminfo[8]);
            boolean sun = Boolean.parseBoolean(alarminfo[9]);
            String memo = "";
            if(alarminfo.length == 11)
                memo = alarminfo[10];

            boolean repeating = mon||tue||wed||thu||fri||sat||sun;

            Calendar currentTime = Calendar.getInstance();
            currentTime.setTimeInMillis(System.currentTimeMillis());

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, min);
            calendar.set(Calendar.SECOND, 0);

            if(currentTime.getTimeInMillis() >= calendar.getTimeInMillis())
                calendar.add(Calendar.HOUR, 24);

            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.putExtra("index", i);
            alarmIntent[i] = PendingIntent.getBroadcast(context, i, intent, 0);

            if(enabled){
                if(repeating)
                    alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent[i]);
                else
                    alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent[i]);
            }
        }

        ComponentName receiver = new ComponentName(context, BootBroadcastReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public void cancelAlarm(Context context){
        TinyDB tinyDB;
        tinyDB = new TinyDB(context.getApplicationContext());
        ArrayList<String> alarms;
        alarms = tinyDB.getListString("alarms");
        if(alarmMgr != null){
            for(int j = 0;j < alarms.size();j++)
                alarmMgr.cancel(alarmIntent[j]);
        }


        ComponentName receiver = new ComponentName(context, BootBroadcastReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

    }
}
