package com.btjcenter.pray4m.api;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by alexmoon on 2015. 11. 17..
 */
public class PicassoImageGetter  implements Html.ImageGetter {

    Resources resources;
    Context context;
    TextView textView;


    public PicassoImageGetter(final Resources res, Context ctx, TextView tv) {
        resources = res;
        context = ctx;
        textView = tv;
    }

    @Override
    public Drawable getDrawable(final String source) {
        final BitmapDrawablePlaceHolder result = new BitmapDrawablePlaceHolder();
        final int width = textView.getWidth();

        new AsyncTask<Void, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(final Void... meh) {
                try {
                    return Picasso.with(context).load(source).resize(width, 0).get();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(final Bitmap bitmap) {
                try {
                    final BitmapDrawable drawable = new BitmapDrawable(resources, bitmap);



                    drawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                    result.setDrawable(drawable);
                    result.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());

                    textView.setText(textView.getText());
                } catch (Exception e) {
                /* nom nom nom*/
                }
            }

        }.execute();

        return result;
    }

    static class BitmapDrawablePlaceHolder extends BitmapDrawable {

        protected Drawable drawable;

        @Override
        public void draw(final Canvas canvas) {
            if (drawable != null) {
                drawable.draw(canvas);
            }
        }

        public void setDrawable(Drawable drawable) {
            this.drawable = drawable;
        }
    }
}