package com.btjcenter.pray4m.api;

import com.btjcenter.pray4m.model.Comment;
import com.btjcenter.pray4m.model.Post;

import java.util.List;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by alexmoon on 2015. 11. 9..
 */
public interface PostService {
    @GET("/api/get_category_posts?status=publish")
    Call<List<Post>> getCatPostList(@Query("slug") String category, @Query("page") Integer page, @Query("count") Integer count);

    @GET("/api/get_category_posts?status=publish&slug=nationsjoin&page=1&count=1&order_by=modified")
    Call<List<Post>> getNationsPostList();

    @GET("/api/recent_nations.php")
    Call<List<Post>> getNewNations();

    @GET("/api/get_post?")
    Call<Post> getPost(@Query("id") Integer id);

    @GET("/api/hot_post.php")
    Call<List<Post>> getHotPost();

    @GET("/api/recent_comments.php")
    Call<List<Post>> getRecentComments();

    @GET("/api/get_search_results?")
    Call<List<Post>> getSearchPostList(@Query("search") String keyword, @Query("page") Integer page);

    @GET("/api/submit_comment?")
    Call<Comment> submitComment(@Query("post_id") Integer id, @Query("name") String name, @Query("email") String email, @Query("content") String content);


    @FormUrlEncoded
    @POST("/api/gap_send.php")
    Call<String> sendGapJoin(
            //@Body GapJoin gapJoin
            @Field("device_id")String device_id,
            @Field("name")String name,
            @Field("age")String age,
            @Field("sex")String sex,
            @Field("country")String country,
            @Field("ip_addr")String ip_addr
            //@Field("regtime")Date regtime
    );

}
