package com.btjcenter.pray4m;

/**
 * Created by alexmoon on 2016. 3. 5..
 */
public class GetNationNum {
    int id;

    public GetNationNum(int _id){
        id=_id;
    }

    public int getNum(){
        int nation_num = 0;
        switch (id) {
            case 8804:nation_num=1;break;
            case 8812:nation_num=2;break;
            case 8810:nation_num=3;break;
            case 8808:nation_num=4;break;
            case 8802:nation_num=5;break;
            case 8614:nation_num=6;break;
            case 8814:nation_num=7;break;
            case 8806:nation_num=8;break;
            case 8618:nation_num=9;break;
            case 8824:nation_num=10;break;
            case 8828:nation_num=11;break;
            case 8832:nation_num=12;break;
            case 8830:nation_num=13;break;
            case 8834:nation_num=14;break;
            case 8816:nation_num=15;break;
            case 8762:nation_num=16;break;
            case 8758:nation_num=17;break;
            case 8753:nation_num=18;break;
            case 8760:nation_num=19;break;
            case 8790:nation_num=20;break;
            case 8792:nation_num=21;break;
            case 8784:nation_num=22;break;
            case 8786:nation_num=23;break;
            case 8788:nation_num=24;break;
            case 8780:nation_num=25;break;
            case 8772:nation_num=26;break;
            case 8775:nation_num=27;break;
            case 8777:nation_num=28;break;
            case 8770:nation_num=29;break;
            case 8765:nation_num=30;break;
            case 8818:nation_num=31;break;
            case 8820:nation_num=32;break;
            case 8822:nation_num=33;break;
            case 8794:nation_num=34;break;
            case 8796:nation_num=35;break;
            case 8798:nation_num=36;break;
            case 8800:nation_num=37;break;
            case 8836:nation_num=38;break;
        }

        return nation_num;
    }
}
