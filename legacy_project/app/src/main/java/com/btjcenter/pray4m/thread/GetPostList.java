package com.btjcenter.pray4m.thread;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.View;
import android.widget.TextView;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.adapter.BigListAdapter;
import com.btjcenter.pray4m.adapter.GridListAdapter;
import com.btjcenter.pray4m.adapter.PostListAdapter;
import com.btjcenter.pray4m.api.ListDeserializer;
import com.btjcenter.pray4m.api.PostService;
import com.btjcenter.pray4m.model.Post;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;



/**
 * Created by alexmoon on 2015. 11. 9..
 */
public class GetPostList extends AsyncTask<Void, Void, Void> {

    Type type;
    Gson gson;
    PostService postService;
    Retrofit retrofit;

    List<Post> posts;
    String category;

    Boolean mLockListView;
    Boolean END_FLAG = false;
    Integer page;

    RecyclerView.Adapter adapter;
    RecyclerView postList;
    LinearLayoutManager layoutManager;
    GridLayoutManager gridManager;
    TextView test;

    View rootView;
    Context context;

    int rId;



    public GetPostList(String cat, View rv, int resId){
        category = cat;
        rootView = rv;
        context = rv.getContext();
        rId = resId;

        postList = (RecyclerView) rootView.findViewById(resId);
        layoutManager = new LinearLayoutManager(context);
        gridManager = new GridLayoutManager(context, 2);
        postList.setHasFixedSize(false);
        if(resId == R.id.postlist_best)
            postList.setLayoutManager(gridManager);
        else
            postList.setLayoutManager(layoutManager);
        //postList.addItemDecoration(new VerticalSpaceItemDecoration(0));
        //postList.addItemDecoration(new DividerItemDecoration(context));
        test = (TextView) rootView.findViewById(R.id.test);
        posts = new ArrayList<Post>();

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(rId == R.id.postlist_best){
            adapter = new GridListAdapter(posts, rootView);
        } else if(rId == R.id.postlist_pds){
            adapter = new BigListAdapter(posts, rootView);
        } else if(rId == R.id.postlist_today){
            adapter = new PostListAdapter(posts, true);
        } else {
            adapter = new PostListAdapter(posts, true);
        }
        postList.setAdapter(adapter);

        type = new TypeToken<List<Post>>(){}.getType();
        gson = new GsonBuilder()
                .registerTypeAdapter(type, new ListDeserializer<>(List.class, "posts")).create();

        retrofit = new Retrofit.Builder()
                .validateEagerly()
                .baseUrl("http://www.btjprayer.net")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        postService = retrofit.create(PostService.class);
        page = 1;

    }

    @Override
    protected Void doInBackground(Void... params) {
        mLockListView = true;

        Call<List<Post>> postscall = postService.getCatPostList(category, page, 10);
        postscall.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Response<List<Post>> response, Retrofit retrofit) {
                if(response != null){
                    if(response.isSuccess()){
                        posts.addAll(response.body());
                        adapter.notifyDataSetChanged();
                        if(response.body().size()==0 || response == null)
                            END_FLAG = true;
                        mLockListView = false;
                        page += 1;
                    }
                }
            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });



        return null;
    }

    @Override
    protected void onPostExecute(Void args){

        postList.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = recyclerView.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                int count = totalItemCount - visibleItemCount;

                if(firstVisibleItem >= count && totalItemCount != 0 && !mLockListView && !END_FLAG) {
                    mLockListView = true;
                    Call<List<Post>> postscall = postService.getCatPostList(category, page, 10);
                    postscall.enqueue(new Callback<List<Post>>() {
                        @Override
                        public void onResponse(Response<List<Post>> response, Retrofit retrofit) {
                            if(response.isSuccess()){
                                posts.addAll(response.body());
                                adapter.notifyDataSetChanged();
                                if (response.body().size() == 0)
                                    END_FLAG = true;
                                mLockListView = false;
                                page += 1;
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable) {

                        }
                    });
                }

            }
        });

    }
}
