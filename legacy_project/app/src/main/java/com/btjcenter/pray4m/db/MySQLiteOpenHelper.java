package com.btjcenter.pray4m.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by alexmoon on 2016. 1. 8..
 */
public class MySQLiteOpenHelper extends SQLiteOpenHelper{

    Context context;

    public MySQLiteOpenHelper(Context ctx) {
        // Database이름은 실제 단말상에서 생성될 파일이름입니다. data/data/package명/databases/DATABASE_NAME식으로 저장
        super(ctx, "gap2.db", null, 1);    // 제일 마지막 인자 : 버젼, 만약 버젼이 높아지면 onUpgrade를 수행한다.
        context =  ctx;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
