package com.btjcenter.pray4m.activity;


import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.btjcenter.pray4m.CloseHandler;
import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.adapter.HomePagerAdapter;
import com.btjcenter.pray4m.utils.DateProvider;

import java.util.Calendar;


public class MainActivity extends MyAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";




    CloseHandler closehandler;                      //class make app finish when press back button twice
    Toolbar toolbar;                                //support.v7 Toolbar replace ActionBar
    static Integer current_nav = R.id.nav_home;                     //current navigationview position
    static Integer current_nav_group = R.id.nav_group1;


    static NavigationView navigationView;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Context context = this;


        //setting closehandler
        closehandler = new CloseHandler(this);

        //setting toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        //setting navigationview
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        toggle.setDrawerIndicatorEnabled(false);
//        toggle.setHomeAsUpIndicator(R.drawable.ic_gapdrawer);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemTextColor(ColorStateList.valueOf(getResources().getColor(android.R.color.white)));
        navigationView.setItemIconTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.white)));
        //setting initial navigationview
        navigationView.getMenu().getItem(0).setChecked(true);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, NavigationholderFragment.newInstance(R.id.nav_home)).commit();
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_main);
        fab.setPadding(30,30,30,30);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GapActivity.class);
                startActivity(intent);
            }
        });

        FloatingActionButton fab_ramadan = (FloatingActionButton) findViewById(R.id.fab_ramadan);
        fab_ramadan.setPadding(15,20,10,20);
        fab_ramadan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RamadanActivity.class);
                startActivity(intent);
            }
        });
        if(Calendar.getInstance().after(new DateProvider().RAMADAN16_END))
            fab_ramadan.setVisibility(View.GONE);

        Uri uri = getIntent().getData();
        if(uri != null) {
            //Log.d(TAG, uri.toString());
            Intent intent = new Intent(this, PostActivity.class);
            intent.putExtra("id", Integer.parseInt(uri.toString().split("=")[1]));

            startActivity(intent);
        }


    }

    @Override
    public void onBackPressed() {
        //close app when drawer is closed
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            closehandler.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            this.startActivity(intent);
            return true;
        } else if (id == R.id.action_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            this.startActivity(intent);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        int gid = item.getGroupId();


        if(current_nav != id){
            if (id == R.id.nav_setting) {
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
            } else if(id == R.id.nav_guide) {
                Intent intent = new Intent(this, AppGuideActivity.class);
                startActivity(intent);
            }
            else {
                Fragment fragment = NavigationholderFragment.newInstance(id);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment)
                        .addToBackStack(null).commit();
                current_nav = id;
            }
        }
        current_nav_group = gid;



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static class NavigationholderFragment extends Fragment {

        private static final String ARG_NAV_ID = "nav_id";
        private static int id;

        TabLayout tabs_home;
        ViewPager pager_home;
        View rootView;


        public static NavigationholderFragment newInstance(int menu_id) {
            id = menu_id;
            NavigationholderFragment fragment = new NavigationholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_NAV_ID, id);
            fragment.setArguments(args);
            return fragment;
        }

        public NavigationholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {


            rootView = inflater.inflate(R.layout.fragment_home, container, false);
            tabs_home = (TabLayout) rootView.findViewById(R.id.tabs_home);
            pager_home = (ViewPager) rootView.findViewById(R.id.pager_home);
            HomePagerAdapter homePagerAdapter = new HomePagerAdapter(getChildFragmentManager());
            pager_home.setAdapter(homePagerAdapter);
            pager_home.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    navigationView.getMenu().getItem(position).setChecked(true);
                    switch (position) {
                        case 0:
                            current_nav = R.id.nav_home; break;
                        case 1:
                            current_nav = R.id.nav_today; break;
                        case 2:
                            current_nav = R.id.nav_nations; break;
                        case 3:
                            current_nav = R.id.nav_pds; break;
                        case 4:
                            current_nav = R.id.nav_prev; break;
                    }
                }

                @Override
                public void onPageSelected(int position) {
                    navigationView.getMenu().getItem(position).setChecked(true);
                    switch (position) {
                        case 0:
                            current_nav = R.id.nav_home; break;
                        case 1:
                            current_nav = R.id.nav_today; break;
                        case 2:
                            current_nav = R.id.nav_nations; break;
                        case 3:
                            current_nav = R.id.nav_pds; break;
                        case 4:
                            current_nav = R.id.nav_prev; break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            tabs_home.setupWithViewPager(pager_home);
            tabs_home.setTabMode(TabLayout.MODE_SCROLLABLE);
            changeTabsFont(tabs_home);

            switch (id){
                case R.id.nav_home:
                    pager_home.setCurrentItem(0);break;
                case R.id.nav_today:
                    pager_home.setCurrentItem(1);break;
                case R.id.nav_nations:
                    pager_home.setCurrentItem(2);break;
//                case R.id.nav_best:
//                    pager_home.setCurrentItem(3);break;
                case R.id.nav_pds:
                    pager_home.setCurrentItem(3);break;
                case R.id.nav_prev:
                    pager_home.setCurrentItem(4);break;
            }


            return rootView;
        }

        private void changeTabsFont(TabLayout tabLayout) {
            Typeface font = Typeface.createFromAsset(rootView.getContext().getAssets(), "fonts/NanumBarunGothic.otf");
            ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
            int tabsCount = vg.getChildCount();
            for (int j = 0; j < tabsCount; j++) {
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        ((TextView) tabViewChild).setTypeface(font, Typeface.NORMAL);
                    }
                }
            }
        }

    }




}


