package com.btjcenter.pray4m.thread;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.btjcenter.pray4m.DividerItemDecoration;
import com.btjcenter.pray4m.GetNationNum;
import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.activity.PostActivity;
import com.btjcenter.pray4m.activity.RamadanActivity;
import com.btjcenter.pray4m.adapter.HomeCommentListAdapter;
import com.btjcenter.pray4m.api.ListDeserializer;
import com.btjcenter.pray4m.api.PostService;
import com.btjcenter.pray4m.model.Comment;
import com.btjcenter.pray4m.model.Post;
import com.btjcenter.pray4m.utils.DateProvider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by alexmoon on 2015. 11. 23..
 */
public class GetHome extends AsyncTask<Void, Void, Void> {

    final public String TAG = "GetHome";

    Type type;
    Gson gson;
    PostService postService;
    Retrofit retrofit;

    List<Post> post_today;
    AutoScrollViewPager viewpager_today;
    TodayPagerAdapter pagerAdapter;
    LinearLayout layout_mark;
    int prev_pager_position;

    List<Post> post_nation;
    CardView cardView_nation1;
    ImageView image_nation1;
    TextView title_nation1;
    CardView cardView_nation2;
    ImageView image_nation2;
    TextView title_nation2;
    int[] image_RId = {
            R.drawable.logo_nation_1,
            R.drawable.logo_nation_2,
            R.drawable.logo_nation_3,
            R.drawable.logo_nation_4,
            R.drawable.logo_nation_5,
            R.drawable.logo_nation_6,
            R.drawable.logo_nation_7,
            R.drawable.logo_nation_8,
            R.drawable.logo_nation_9,
            R.drawable.logo_nation_10,
            R.drawable.logo_nation_11,
            R.drawable.logo_nation_12,
            R.drawable.logo_nation_13,
            R.drawable.logo_nation_14,
            R.drawable.logo_nation_15,
            R.drawable.logo_nation_16,
            R.drawable.logo_nation_17,
            R.drawable.logo_nation_18,
            R.drawable.logo_nation_19,
            R.drawable.logo_nation_20,
            R.drawable.logo_nation_21,
            R.drawable.logo_nation_22,
            R.drawable.logo_nation_23,
            R.drawable.logo_nation_24,
            R.drawable.logo_nation_25,
            R.drawable.logo_nation_26,
            R.drawable.logo_nation_27,
            R.drawable.logo_nation_28,
            R.drawable.logo_nation_29,
            R.drawable.logo_nation_30,
            R.drawable.logo_nation_31,
            R.drawable.logo_nation_32,
            R.drawable.logo_nation_33,
            R.drawable.logo_nation_34,
            R.drawable.logo_nation_35,
            R.drawable.logo_nation_36,
            R.drawable.logo_nation_37,
            R.drawable.logo_nation_38,
    };

    List<Comment> recent_comments;
    RecyclerView recycler_comments;
    LinearLayoutManager layoutManager;
    HomeCommentListAdapter commentListAdapter;

    View rootView;
    Context context;




    public GetHome(View rv){
        rootView = rv;
        context = rv.getContext();

        viewpager_today = (AutoScrollViewPager) rootView.findViewById(R.id.viewpager_hometoday);
        post_today = new ArrayList<Post>();

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        layout_mark = (LinearLayout) rootView.findViewById(R.id.layout_viewpager_mark);

        cardView_nation1 = (CardView) rootView.findViewById(R.id.layout_homenation1);
        image_nation1 = (ImageView)rootView.findViewById(R.id.image_homenation1);
        title_nation1 = (TextView)rootView.findViewById(R.id.title_homenation1);
//        cardView_nation2 = (CardView) rootView.findViewById(R.id.layout_homenation2);
//        image_nation2 = (ImageView)rootView.findViewById(R.id.image_homenation2);
//        title_nation2 = (TextView)rootView.findViewById(R.id.title_homenation2);
        post_nation = new ArrayList<Post>();

        recycler_comments = (RecyclerView) rootView.findViewById(R.id.recycler_comments_home);
        layoutManager = new LinearLayoutManager(context);
        recycler_comments.setHasFixedSize(false);
        recycler_comments.setLayoutManager(layoutManager);
        recycler_comments.addItemDecoration(new DividerItemDecoration(context));
        recent_comments = new ArrayList<Comment>();

        commentListAdapter = new HomeCommentListAdapter(recent_comments);
        recycler_comments.setAdapter(commentListAdapter);

        type = new TypeToken<List<Post>>(){}.getType();
        gson = new GsonBuilder()
                .registerTypeAdapter(type, new ListDeserializer<>(List.class, "posts")).create();

        retrofit = new Retrofit.Builder()
                .validateEagerly()
                .baseUrl("http://www.btjprayer.net")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        postService = retrofit.create(PostService.class);
    }

    @Override
    protected Void doInBackground(Void... params) {
        Call<List<Post>> postscall_today = postService.getCatPostList("today", 1, 2);
        postscall_today.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(final Response<List<Post>> response, Retrofit retrofit) {
                if(response.isSuccess()&&response.body().size()!=0){
                    Log.d(TAG, "Today Posts : " + response.body().size());
                    post_today.addAll(response.body());
                    pagerAdapter = new TodayPagerAdapter(post_today);
                    viewpager_today.setAdapter(pagerAdapter);
                    Log.d(TAG, "Today Posts : " + post_today.size());
                    pagerAdapter.notifyDataSetChanged();
                    initPageMark(post_today.size());
                    viewpager_today.setInterval(5000);
                    viewpager_today.startAutoScroll(5000);
                }
            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });


        Call<List<Post>> postscall_nations = postService.getNewNations();
        postscall_nations.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(final Response<List<Post>> response, Retrofit retrofit) {
                if(response.isSuccess()&&response.body().size()!=0){
//                    Picasso.with(context)
//                            .load(response.body().get(0).getAttachments().get(0).getUrl())
//                            .placeholder(R.drawable.temp)
//                            .into(image_nation1);

                    image_nation1.setImageResource(image_RId[new GetNationNum(response.body().get(0).getId()).getNum() - 1]);

                    String title1 = "";
                    if(response.body().get(0).getTitle().split(";").length == 2)
                        title1 = response.body().get(0).getTitle().split(";")[1];
                    title_nation1.setText(title1);
                    cardView_nation1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, PostActivity.class);
                            intent.putExtra("id", response.body().get(0).getId());
                            intent.putExtra("nation", true);
                            context.startActivity(intent);
                        }
                    });

//                    image_nation2.setImageResource(image_RId[new GetNationNum(response.body().get(1).getId()).getNum() - 1]);
//
//                    String title2 = "";
//                    if(response.body().get(1).getTitle().split(";").length == 2)
//                        title2 = response.body().get(1).getTitle().split(";")[1];
//                    title_nation2.setText(title2);
//                    cardView_nation2.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent intent = new Intent(context, PostActivity.class);
//                            intent.putExtra("id", response.body().get(1).getId());
//                            intent.putExtra("nation", true);
//                            context.startActivity(intent);
//                        }
//                    });

                }
            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });

        Call<List<Post>> postcall_comments = postService.getRecentComments();
        postcall_comments.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Response<List<Post>> response, Retrofit retrofit) {
                if (response.isSuccess() && response.body().get(0).getComments().size() != 0) {
                    Log.d(TAG, "Recent Comments : " + response.body().get(0).getComments().size());
                    Log.d(TAG, "Recent Comments : " + response.body().get(0).getComments().get(0).getContent());
                    recent_comments.addAll(response.body().get(0).getComments());
                    commentListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d(TAG, "Recent Comment failed");
            }
        });

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {


        Log.d(TAG, "Today Posts : " + post_today.size());
        viewpager_today.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(layout_mark.getChildAt(prev_pager_position) != null & layout_mark.getChildAt(position) != null) {
                    layout_mark.getChildAt(prev_pager_position).setBackgroundResource(R.drawable.drawable_dot_not_select);
                    layout_mark.getChildAt(position).setBackgroundResource(R.drawable.drawable_dot_select);
                }
                prev_pager_position = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void initPageMark(int size) {
        for (int i = 0; i < size; i++) {
            ImageView iv = new ImageView(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(10,3,10,3);
            iv.setLayoutParams(layoutParams);
            if (i == 0)
                iv.setBackgroundResource(R.drawable.drawable_dot_select);
            else
                iv.setBackgroundResource(R.drawable.drawable_dot_not_select);

            layout_mark.addView(iv);
        }
        prev_pager_position = 0;
    }

    class TodayPagerAdapter extends PagerAdapter {
        List<Post> posts;

        TodayPagerAdapter(List<Post> _posts){
            posts = new ArrayList<>();
            posts.addAll(_posts);
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemview = LayoutInflater.from(context).inflate(R.layout.item_pager_main_today, null);



            LinearLayout layout_bar = (LinearLayout) itemview.findViewById(R.id.title_bar);
            ImageView image = (ImageView) itemview.findViewById(R.id.main_image);
            TextView title = (TextView) itemview.findViewById(R.id.title);
            TextView date = (TextView) itemview.findViewById(R.id.date);
            TextView tag = (TextView) itemview.findViewById(R.id.tag);

            if(Calendar.getInstance().before(new DateProvider().RAMADAN16_END)) {
                if(position == 0) {
                    image.setImageResource(R.drawable.slide_ramadan);
                    tag.setText("[라마단]");
                    date.setText("6월 6일 ~ 7월 5일");
                    title.setText("라마단이란?");
                    itemview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, PostActivity.class);
                            intent.putExtra("id", 12662);
                            context.startActivity(intent);
                        }
                    });
                } else {
                    if(posts.get(position - 1).getAttachments().size() != 0) {
                        Picasso.with(context)
                                .load(posts.get(position - 1).getAttachments().get(0).getUrl())
                                .placeholder(R.drawable.temp)
                                .into(image);
                    } else {
                        image.setImageResource(R.drawable.temp);
                    }
                    title.setText(posts.get(position - 1).getTitle_plain());
                    String date_string = posts.get(position - 1).getDate();
                    int month = Integer.parseInt(date_string.substring(5, 7));
                    int day = Integer.parseInt(date_string.substring(8, 10));
                    date_string = month + "월 " + day + "일";
                    date.setText(date_string);

                    itemview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, PostActivity.class);
                            intent.putExtra("id", posts.get(position - 1).getId());
                            context.startActivity(intent);
                        }
                    });
                }
            } else {
                if(posts.get(position).getAttachments().size() != 0) {
                    Picasso.with(context)
                            .load(posts.get(position).getAttachments().get(0).getUrl())
                            .placeholder(R.drawable.temp)
                            .into(image);
                } else {
                    image.setImageResource(R.drawable.temp);
                }
                title.setText(posts.get(position).getTitle_plain());
                String date_string = posts.get(position).getDate();
                int month = Integer.parseInt(date_string.substring(5, 7));
                int day = Integer.parseInt(date_string.substring(8, 10));
                date_string = month + "월 " + day + "일";
                date.setText(date_string);

                itemview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, PostActivity.class);
                        intent.putExtra("id", posts.get(position).getId());
                        context.startActivity(intent);
                    }
                });
            }
            container.addView(itemview);
            return itemview;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View)object);
        }

        @Override
        public int getCount() {
            return posts.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }




}
