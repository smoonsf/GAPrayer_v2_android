package com.btjcenter.pray4m.model;

import java.util.List;

/**
 * Created by alexmoon on 2015. 11. 7..
 */
public class Post {
    Integer id;
    String slug;
    String url;
    String title;
    String title_plain;
    String content;
    String date;
    String modified;
    List<Category> categories;
    List<Tag> tags;
    Author author;
    List<Comment> comments;
    List<Attachment> attachments;
    Integer comment_count;
    String comment_status;


    public Integer getId() {return id;}
    public String getSlug() {return slug;}
    public String getUrl() {return url;}
    public String getTitle() {return title
            .replaceAll("(&)(#)8216;", "'")
            .replaceAll("(&)(#)8217;", "'")
            .replaceAll("(&)(#)8220;", "\"")
            .replaceAll("(&)(#)8221;", "\"")
            .replaceAll("(&)(#)8230;", "...");}
    public String getTitle_plain() {return title_plain
            .replaceAll("(&)(#)8216;", "'")
            .replaceAll("(&)(#)8217;", "'")
            .replaceAll("(&)(#)8220;", "\"")
            .replaceAll("(&)(#)8221;", "\"")
            .replaceAll("(&)(#)8230;", "...");}
    public String getContent() {return content;}
    public String getDate() {return date;}
    public String getModified() {return modified;}
    public List<Category> getCategories() {return categories;}
    public List<Tag> getTags() {return tags;}
    public Author getAuthor() {return author;}
    public List<Comment> getComments() {return comments;}
    public List<Attachment> getAttachments() {return attachments;}
    public Integer getComment_count() {return comment_count;}
    public String getComment_status() {return comment_status;}

}
