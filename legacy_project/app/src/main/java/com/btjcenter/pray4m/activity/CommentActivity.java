package com.btjcenter.pray4m.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.api.ListDeserializer;
import com.btjcenter.pray4m.api.PostService;
import com.btjcenter.pray4m.model.Comment;
import com.btjcenter.pray4m.model.Post;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class CommentActivity extends MyAppCompatActivity {

    final private int LANG_KOR = 0;
    final private int LANG_ENG = 1;
    final private int LANG_CN = 2;
    final private int LANG_JAP = 3;
    private int language_mode = 0;

    final String PREF_NAME_COMMENT = "name_comment";
    final String PREF_EMAIL_COMMENT = "email_comment";



    Button button_ok;

    TextView textView_title;

    EditText editText_name;
    EditText editText_email;
    EditText editText_content;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        language_mode = getIntent().getIntExtra("LANG_MODE", 0);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.97);
        getWindow().setLayout(screenWidth, WindowManager.LayoutParams.MATCH_PARENT);

        final int id = getIntent().getIntExtra("id", 0);
        final Context ctx = this;

        textView_title = (TextView) findViewById(R.id.textview_title_comment);
        button_ok = (Button)findViewById(R.id.button_ok_comment);
        editText_name = (EditText)findViewById(R.id.edittext_name_comment);
        editText_name.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        editText_email = (EditText)findViewById(R.id.edittext_email_comment);
        editText_email.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        editText_content = (EditText)findViewById(R.id.edittext_content_comment);

        editText_name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    editText_email.requestFocus();
                    return true;
                }

                return false;
            }
        });
        editText_email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT) {
                    editText_content.requestFocus();
                    return true;
                }

                return false;
            }
        });


        editText_name.setText(sharedPreferences.getString(PREF_NAME_COMMENT, ""));
        editText_email.setText(sharedPreferences.getString(PREF_EMAIL_COMMENT, ""));


        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText_name.getText().length() != 0 && isEmailValid(editText_email.getText().toString()) &&
                        editText_content.getText().length() != 0 && editText_content.getText().toString().length() <= 300){
                    new SubmitComment(id, editText_name.getText().toString(), editText_email.getText().toString(),
                            editText_content.getText().toString()).execute();
                    sharedPreferences.edit().putString(PREF_NAME_COMMENT, editText_name.getText().toString()).apply();
                    sharedPreferences.edit().putString(PREF_EMAIL_COMMENT, editText_email.getText().toString()).apply();
                    Toast.makeText(ctx, "관리자 승인 후에 기도문이 등록됩니다", Toast.LENGTH_SHORT);
                    finish();
                } else {
                    if(editText_name.getText().length() == 0 || editText_email.getText().length() == 0){
                        Toast.makeText(ctx, "이름 및 이메일 주소를 입력해주세요", Toast.LENGTH_SHORT).show();
                    } else if(editText_content.getText().length() == 0) {
                        Toast.makeText(ctx, "기도문을 작성해주세요", Toast.LENGTH_SHORT).show();
                    } else if(editText_content.getText().length() > 300) {
                        Toast.makeText(ctx, "300자 이하로 기도문을 작성해 주세요. 현재 글자 수 : " + editText_content.getText().length(), Toast.LENGTH_SHORT).show();
                    } else if(isEmailValid(editText_email.getText().toString())){
                        Toast.makeText(ctx, "이메일 형식을 확인해주세요", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

        switch (language_mode) {
            case LANG_ENG:
                textView_title.setText("Share Prayer Request");
                editText_name.setHint("Name");
                editText_email.setHint("Email");
                editText_content.setHint("Prayer Request");
                button_ok.setText("Share");
                break;
            case LANG_CN:
                textView_title.setText("写代祷文");
                editText_name.setHint("姓名");
                editText_email.setHint("邮箱");
                editText_content.setHint("祷告文");
                button_ok.setText("发表");
                break;
            case LANG_JAP:
                textView_title.setText("祈りの文の投稿");
                editText_name.setHint("お名前");
                editText_email.setHint("メールアドレス");
                editText_content.setHint("お祈り");
                button_ok.setText("投稿する");
                break;
        }

    }

    class SubmitComment extends AsyncTask<Void,Void,Void> {

        int id;
        String name;
        String email;
        String content;

        Type type;
        Gson gson;
        PostService postService;
        Retrofit retrofit;

        public SubmitComment(int _id, String _name, String _email, String _content){
            id = _id;
            name = _name;
            email = _email;
            content = _content;
        }

        @Override
        protected void onPreExecute() {
            type = new TypeToken<List<Post>>(){}.getType();
            gson = new GsonBuilder()
                    .registerTypeAdapter(type, new ListDeserializer<>(List.class, "posts")).create();

            retrofit = new Retrofit.Builder()
                    .validateEagerly()
                    .baseUrl("http://www.btjprayer.net")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            postService = retrofit.create(PostService.class);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Call<Comment> call_comment = postService.submitComment(id, name, email, content);
            call_comment.enqueue(new Callback<Comment>() {
                @Override
                public void onResponse(Response<Comment> response, Retrofit retrofit) {

                }

                @Override
                public void onFailure(Throwable throwable) {

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
}
