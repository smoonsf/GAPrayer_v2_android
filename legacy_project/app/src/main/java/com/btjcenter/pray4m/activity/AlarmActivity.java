package com.btjcenter.pray4m.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.btjcenter.pray4m.AlarmReceiver;
import com.btjcenter.pray4m.DividerItemDecoration;
import com.btjcenter.pray4m.R;
import com.btjcenter.pray4m.adapter.AlarmListAdapter;
import com.btjcenter.pray4m.db.TinyDB;

import java.util.ArrayList;

public class AlarmActivity extends MyAppCompatActivity {
    static public String TAG = "AlarmActivity";

    RecyclerView alarmlist;
    ArrayList<String> alarms;
    LinearLayoutManager layoutManager;
    AlarmListAdapter alarmListAdapter;



    static ImageView button_delete;
    static Button button_add;

    boolean DELETE_MODE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        final Context ctx = this;



    }

    @Override
    protected void onResume() {
        super.onResume();
        final Context ctx = this;

        DELETE_MODE = false;

        alarmlist = (RecyclerView) findViewById(R.id.alarmlist);
        layoutManager = new LinearLayoutManager(this);
        alarmlist.setHasFixedSize(true);
        alarmlist.setLayoutManager(layoutManager);
        alarmlist.addItemDecoration(new DividerItemDecoration(this));

        TinyDB tinyDB = new TinyDB(getApplicationContext());
        alarms = tinyDB.getListString("alarms");
        //Log.d(TAG, "" + alarms.size());



        alarmListAdapter = new AlarmListAdapter(this);
        alarmlist.setAdapter(alarmListAdapter);

        button_add = (Button) findViewById(R.id.button_add_alarm);
        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, SetAlarmActivity.class);
                startActivity(intent);
            }
        });

        button_delete = (ImageView) findViewById(R.id.image_delete_alarm);
        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DELETE_MODE) {
                    DELETE_MODE = false;
                }
                else {
                    DELETE_MODE = true;
                }
                Intent intent = new Intent(v.getContext(), DeleteReceiver.class);
                intent.putExtra("delete_mode", DELETE_MODE);
                sendBroadcast(intent);

                Intent toList = new Intent("ENABLE_CHECK");
                toList.putExtra("delete_mode", DELETE_MODE);
                sendBroadcast(toList);

                if(!DELETE_MODE)
                    onResume();
            }
        });
    }

    static public class DeleteReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, Intent intent) {
            boolean d_mode = intent.getBooleanExtra("delete_mode", false);
            if(d_mode) {
                button_add.setText("알람 삭제");
                button_add.setBackgroundColor(Color.parseColor("#BABABA"));

                button_add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TinyDB tinyDB = new TinyDB(context.getApplicationContext());
                        ArrayList<Boolean> delete_list = tinyDB.getListBoolean("delete_list");
                        ArrayList<String> alarms = tinyDB.getListString("alarms");
                        for(int j = delete_list.size() - 1;j >= 0;j--){
                            if(delete_list.get(j)) {
                                alarms.remove(j);
                                delete_list.remove(j);
                            }
                        }
                        tinyDB.putListString("alarms", alarms);
                        tinyDB.putListBoolean("delete_list", delete_list);


                        AlarmReceiver alarmReceiver = new AlarmReceiver();
                        alarmReceiver.cancelAlarm(context.getApplicationContext());
                        alarmReceiver.setAlarm(context.getApplicationContext());
                        button_delete.performClick();
                    }
                });
            } else {
                button_add.setText("+알람 추가");
                button_add.setBackgroundColor(Color.parseColor("#FABEBA"));;

                button_add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, SetAlarmActivity.class);
                        context.startActivity(intent);
                    }
                });

            }


        }
    }
}
