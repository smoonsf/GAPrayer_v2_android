package com.btjcenter.pray4m.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by alexmoon on 2016. 6. 2..
 */
public class DateProvider {
    final public Calendar RAMADAN16_START = new GregorianCalendar(2016, 5, 6);
    final public Calendar RAMADAN16_END = new GregorianCalendar(2016, 6, 6);
}
