package com.btjcenter.pray4m;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.btjcenter.pray4m.adapter.BibleListAdapter;

import java.util.ArrayList;

/**
 * Created by alexmoon on 2015. 11. 13..
 */
public class BiblePageholderFragment extends Fragment{

    int init;
    ArrayList<String> verse;
    boolean gap_table = false;


    public static BiblePageholderFragment create(){
        BiblePageholderFragment fragment = new BiblePageholderFragment();
        Bundle args = new Bundle();
        args.putBoolean("gaptable", true);
        fragment.setArguments(args);
        return fragment;
    }

    public static BiblePageholderFragment create(int _init, ArrayList<String> _verses){
        BiblePageholderFragment fragment = new BiblePageholderFragment();
        Bundle args = new Bundle();
        args.putInt("init", _init);
        args.putStringArrayList("verses", _verses);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init = getArguments().getInt("init");
        verse = getArguments().getStringArrayList("verses");
        gap_table = getArguments().getBoolean("gaptable", false);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = null;

        if(gap_table){
            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_gaptable, container, false);

        } else {
            rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_bible, container, false);
            RecyclerView biblelist = (RecyclerView) rootView.findViewById(R.id.recycler_bible);
            LinearLayoutManager layoutManager = new LinearLayoutManager(rootView.getContext());
            biblelist.setLayoutManager(layoutManager);
            BibleListAdapter bibleListAdapter = new BibleListAdapter(init, verse);
            biblelist.setAdapter(bibleListAdapter);
        }




        return rootView;
    }


}
